<html>
	<head>
		<title>create Password</title>
		
		<link href='//fonts.googleapis.com/css?family=Lato:100' rel='stylesheet' type='text/css'>

		<style>
			body {
				margin: 0;
				padding: 0;
				width: 100%;
				height: 100%;
				color: #B0BEC5;
				display: table;
				font-weight: 100;
				font-family: 'Lato';
			}

			.container {
				text-align: center;
				display: table-cell;
				vertical-align: middle;
			}

			.content {
				text-align: center;
				display: inline-block;
			}

			.title {
				font-size: 96px;
				margin-bottom: 40px;
			}

			.quote {
				font-size: 24px;
			}
		</style>
	</head>
	<body>
		<div class="container">
                <form method="POST" action="">
				<div class="form">
                    <div class="form-group">
                        <span>Password</span>
                    </div>
                    <div class="form-group">
                        <input type='password' class="btn btn-lg" />
                    </div>
                    <div class="form-group">
                        <span>Confirm Password</span>
                    </div>
                    <div class="form-group">
                        <input type='password' class="btn btn-lg" />
                        <input type='hidden' class="btn btn-lg" value="{!! $key !!}" />
                    </div>
                    <div class="form-group">
                        <button type='submit' method="" class="btn btn-lg">Submit</button>
                    </div>
                </div>
				<div class="quote">{{ Inspiring::quote() }}</div>
		</div>
	</body>
</html>
