<?php namespace Larasite\Http\Controllers;

use Larasite\Http\Requests;
use Larasite\Http\Controllers\Controller;
use adLDAP\adLDAP;
use Illuminate\Http\Request;
use Larasite\User;
use Illuminate\Cookie\CookieJar;

class AuthController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public $restfull = true;

	public function token(CookieJar $cookieJar, Request $request)
	{
		$csrf_token = csrf_token();
		\Session::flush();
        return \Response::json(['token' => $csrf_token,'msg'=>'success'],200);
	}
	// ######################################################################
	public function doLogin(CookieJar $cookieJar, Request $request){ 
		$username = \Input::get('name');
		$password = \Input::get('pass');
		$token = \Input::get('auth');

		// cek input tidak null
		if(isset($username,$password)){ // check input not null
			
			//$sess_token = \Cookie::get('token');
			if($token){ // check
					try{
						$ldap = new adLDAP;
						//$check_ad = \DB::select("select c.role_name ,a.employee_id from emp a, ldap b, role c where b.employee_id = a.employee_id and b.role_id = c.role_id and a.ad_username = '$username' and b.active = 0");
						$check_ad = \DB::table('view_nonactive_login')->where('ad_username','=',$username)->get(['ad_username','role_name','employee_id','username']);
						//echo 'dimari';
						if(!empty($check_ad)){

							$i = array();
							foreach ($check_ad as $key) {
								$i['role_name'] = $key->role_name;
								$i['name'] = $key->username;
								$i['employee_id'] = $key->employee_id;
								$i['ad_username'] = $key->ad_username;
							} 	
							if($ldap->authenticate($i['ad_username'],$password)){
								$db = new User;
								$id = $i['employee_id'];
								$encode = $this->get_current_token($token,$id,1);
								$data =  $db->auth_ldap($i['employee_id'],$encode);
								//echo $data;
								if(isset($data)){
									$message = 'access granted.';
									$status = 200;
									if($i['role_name'] == 'user'){
										$result = ['username'=>$i['name'],'key'=>$encode,'picture'=>null,'access'=>['name'=>$i['role_name'],'id'=>$id],'emp'=>null];
									}
									else{
										$result = ['username'=>$i['name'],'key'=>$encode,'picture'=>null,'access'=>null,'emp'=>$i['employee_id']];
									}
									
								}else{
									$message = 'access denaid.';
									$status = 500;
									$result = null;
								}
								return \Response::json(['header'=>['message'=>$message,'status'=>$status],'data'=>$result],$status);
							}
							else{
								$msg = 'Nama tidak dikenali, mohon hubungi administrator';
								return \Response::json(['header'=>['name'=>$username,'message'=>$msg,'status'=>404]],404);
							}	
						}else{
							$msg = 'Nama tidak dikenali, mohon hubungi administrator1';
							return \Response::json(['header'=>['name'=>$username,'message'=>$msg,'status'=>404]],404);

						}
						
						
					}
					catch(adLDAPException $e){
						return  $e; 
					}
			}
			else{
				$msg = 'error token';
				return \Response::json(['name'=>$username,'message'=>$msg,'status'=>403],403);
			}
		}
		else{
			$msg = 'Input tidak boleh kosong';
			return \Response::json(['message'=>$msg],303);
		}
	} // end Function doLogin #########################################################################

	public function logout(){
		$req = \Request::all();
		$db = new User;
		if(isset($req['key'])){
			$cek = $db->logout($req['key']);
			if(isset($cek)){
				$message = 'Bye.. Bye..';
				$status = 200;
			}
			else{
				$message = 'Not Found.';
				$status = 403;
			}
		}
		else{
			$message = 'ID Not Found';
			$status = 404;
		}
		return \Response::json(['header'=>['message'=>$message,'status'=>$status]],$status);
	}



	public function get_current_token($key,$id,$type){
		// $q = \DB::select("select b.employee_id from emp a, ldap b where a.employee_id = b.employee_id and a.employee_id = '$username'");
		// $r = array();
		// foreach ($q as $keys) {
		// 	$r['employee_id'] = $keys->employee_id;
		// }
		$token_base64 = base64_encode($key.'-'.$id);
		$decode_base64 = base64_decode($token_base64);
		$data =  ['encode'=>$token_base64,'decode'=>$decode_base64];
		
		if($type){
			return $data['encode'];
		}
		else{
			return $data['decode'];	
		}
		
	}

}
