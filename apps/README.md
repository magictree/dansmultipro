## Description

-   Restfull API
-   Authenticate with JWT
-   Laravel 5.0
-   MySQL 5.6
-   API DOC : https://documenter.getpostman.com/view/47081/TVzNJKh4
-   Git : https://gitlab.com/magictree/dansmultipro.git
