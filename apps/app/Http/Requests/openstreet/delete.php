<?php namespace Larasite\Http\Requests\openstreet;

use Larasite\Http\Requests\Request;

class delete extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return false;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			"single"=>[
				"data"=>[
					"id"=> 1
				],
				"type"=> "numeric"
			],
			"multi"=>[
				"data"=>[
					"id"=> [1,2,3,4]
				],
				"type"=> "array"
			]
		];
	}

}
