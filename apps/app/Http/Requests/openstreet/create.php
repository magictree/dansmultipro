<?php namespace Larasite\Http\Requests\openstreet;

use Larasite\Http\Requests\Request;
use Larasite\Library\LibRegex;

class create extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function example_json_create(){
		return [
			"data.no_ruas"=>"00001",
			"data.id_kecamatan"=>1,
			"data.name"=>"JL. IKAN TONGKOL LAHI LAI BISI  KOPAN",
			"data.patok_sta"=>"STA 0+000 - 0+150",
			"data.geo_dms"=>"-10°9'44,821\"S - 123°34'34,047\"E",
			"data.geo_decimal"=>"-10.944821,123.3434047",
			"data.panjang_jalan"=>150,
			"data.lebar_jalan"=>8,

			"data.detil_permukaan.susunan"=>1,
			"data.detil_permukaan.kondisi"=>1,
			"data.detil_permukaan.penurunan"=>"< 10 % LUAS",
			"data.detil_permukaan.tambalan"=>"< 10 % LUAS",

			"data.detil_kerusakan.detil_keretakan.jenis"=>1,
			"data.detil_kerusakan.detil_keretakan.lebar"=>1,
			"data.detil_kerusakan.detil_keretakan.luas"=>"< 10 % LUAS",

			"data.detil_kerusakan.detil_kerusakan_lain.jumlah_lubang"=>1,
			"data.detil_kerusakan.detil_kerusakan_lain.ukuran_lubang"=>2,
			"data.detil_kerusakan.detil_kerusakan_lain.bekas_roda"=>"< 10 % LUAS",
			"data.detil_kerusakan.detil_kerusakan_lain.kerusakan_tepi_kiri"=>"< 10 % LUAS",
			"data.detil_kerusakan.detil_kerusakan_lain.kerusakan_tepi_kanan"=>"< 10 % LUAS",

			"data.detil_kerusakan.detil_bahu_jalan.bahu_jalan_kiri"=>1,
			"data.detil_kerusakan.detil_bahu_jalan.bahu_jalan_kanan"=>1,
			"data.detil_kerusakan.detil_bahu_jalan.permukaan_bahu_kiri"=>1,
			"data.detil_kerusakan.detil_bahu_jalan.permukaan_bahu_kanan"=>1,
			"data.detil_kerusakan.detil_bahu_jalan.kondisi_saluran_kiri"=>1,
			"data.detil_kerusakan.detil_bahu_jalan.kondisi_saluran_kanan"=>1,
			"data.detil_kerusakan.detil_bahu_jalan.lereng_kiri"=>1,
			"data.detil_kerusakan.detil_bahu_jalan.lereng_kanan"=>1,
			"data.detil_kerusakan.detil_bahu_jalan.trotoar_kiri"=>1,
			"data.detil_kerusakan.detil_bahu_jalan.trotoar_kanan"=>1,

			"data.kondisi_ruas"=>1,
			"data.penanganan_ruas"=>2,
			"data.jenis_kekerasan"=>2,
			"type"=>"object"
		];
	}

	public function default_value_detail(){
		return [
			'detil_permukaan' => [
				"susunan"=> null,
				"kondisi"=> null,
				"penurunan"=> null,
				"tambalan"=> null
			],
		
			'detil_kerusakan' => [
				"detil_keretakan"=> [
					"jenis"=> null,
					"lebar"=> null,
					"luas"=> null
				],
				"detil_kerusakan_lain"=> [
					"jumlah_lubang"=> null,
					"ukuran_lubang"=> null,
					"bekas_roda"=> null,
					"kerusakan_tepi_kiri"=> null,
					"kerusakan_tepi_kanan"=> null
				],
				"kondisi_bahu_jalan"=> [
					"bahu_jalan_kiri"=> null,
					"bahu_jalan_kanan"=> null,
					"permukaan_bahu_kiri"=> null,
					"permukaan_bahu_kanan"=> null,
					"kondisi_saluran_kiri"=> null,
					"kondisi_saluran_kanan"=> null,
					"lereng_kiri"=> null,
					"lereng_kanan"=> null,
					"trotoar_kiri"=> null,
					"trotoar_kanan"=> null
				]
			],
		
			'detil_bahu_jalan' => [
				"trotoar"=> [
					"p"=> null,
					"l"=>null,
					"t"=> null
				],
				"selokan"=> [
					"kiri"=> [
						"p"=> null,
						"l"=> null,
						"t"=> null
					],
					"kanan"=> [
						"p"=> null,
						"l"=> null,
						"t"=> null
					]
				],
				"lebar_bahu_jalan"=> [
					"kiri"=> null,
					"kanan"=> null
				]
			],
			"kondisi_ruas"=>0,
			"penanganan_ruas"=>0,
			"jenis_kekerasan"=>0,
			"klasifikasi_status"=>0,
			"klasifikasi_fungsi"=>0

		];
	}

	public function rules(){
		$reg = \LibRegex::get_instance();
		return 
		[
			'data.no_ruas'=>"required|alpha_num|min:4|max:30",
			'data.id_kecamatan'=>'required|numeric',
			'data.nama_ruas'=>'required|'.$reg['name'].'|min:4|max:50',
			'data.patok_sta'=>'required|'.$reg['sta'].'|min:4|max:25',
			'data.geo_dms'=>'required|string|min:6|max:50',
			'data.geo_decimal'=>'required|'.$reg['latlon'].'|min:6|max:50',
			'data.panjang_jalan'=>'required|numeric',
			'data.lebar_jalan'=>'required|numeric',
			
			"data.detil_bahu_jalan.trotoar.p" => 'numeric',
			"data.detil_bahu_jalan.trotoar.l" => 'numeric',
			"data.detil_bahu_jalan.trotoar.t" => 'numeric',
			
			"data.detil_bahu_jalan.selokan.kiri.p" => 'numeric',
			"data.detil_bahu_jalan.selokan.kiri.l" => 'numeric',
			"data.detil_bahu_jalan.selokan.kiri.t" => 'numeric',

			"data.detil_bahu_jalan.selokan.kanan.p" => 'numeric',
			"data.detil_bahu_jalan.selokan.kanan.l" => 'numeric',
			"data.detil_bahu_jalan.selokan.kanan.t" => 'numeric',

			"data.detil_bahu_jalan.lebar_bahu_jalan.kiri" => 'numeric',
			"data.detil_bahu_jalan.lebar_bahu_jalan.kanan" => 'numeric',

			'data.detil_permukaan.susunan'=>'numeric',
			'data.detil_permukaan.kondisi'=>'numeric',
			'data.detil_permukaan.penurunan'=>'string',
			'data.detil_permukaan.tambalan'=>'string',

			'data.detil_kerusakan.detil_keretakan.jenis'=>'numeric',
			'data.detil_kerusakan.detil_keretakan.lebar'=>'numeric',
			'data.detil_kerusakan.detil_keretakan.luas'=>'string',

			'data.detil_kerusakan.detil_kerusakan_lain.jumlah_lubang'=>'string',
			'data.detil_kerusakan.detil_kerusakan_lain.ukuran_lubang'=>'string',
			'data.detil_kerusakan.detil_kerusakan_lain.bekas_roda'=>'string',
			'data.detil_kerusakan.detil_kerusakan_lain.kerusakan_tepi_kiri'=>'string',
			'data.detil_kerusakan.detil_kerusakan_lain.kerusakan_tepi_kanan'=>'string',

			'data.detil_kerusakan.kondisi_bahu_jalan.bahu_jalan_kiri'=>'numeric',
			'data.detil_kerusakan.kondisi_bahu_jalan.bahu_jalan_kanan'=>'numeric',
			'data.detil_kerusakan.kondisi_bahu_jalan.permukaan_bahu_kiri'=>'numeric',
			'data.detil_kerusakan.kondisi_bahu_jalan.permukaan_bahu_kanan'=>'numeric',
			'data.detil_kerusakan.kondisi_bahu_jalan.kondisi_saluran_kiri'=>'numeric',
			'data.detil_kerusakan.kondisi_bahu_jalan.kondisi_saluran_kanan'=>'numeric',
			'data.detil_kerusakan.kondisi_bahu_jalan.lereng_kiri'=>'numeric',
			'data.detil_kerusakan.kondisi_bahu_jalan.lereng_kanan'=>'numeric',
			'data.detil_kerusakan.kondisi_bahu_jalan.trotoar_kiri'=>'numeric',
			'data.detil_kerusakan.kondisi_bahu_jalan.trotoar_kanan'=>'numeric',

			'data.kondisi_ruas'=>'numeric',
			'data.penanganan_ruas'=>'numeric',
			'data.jenis_kekerasan'=>'numeric',
			"data.klasifikasi_status"=>'numeric',
			"data.klasifikasi_fungsi"=>'numeric'
		];
	}

}
