<?php namespace Larasite\Http\Requests\openstreet;

use Larasite\Http\Requests\Request;

class show extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return false;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			"data"=>[
				'id'=>'required|numeric',
			],
			"type"=>"object"
		];
	}

}
