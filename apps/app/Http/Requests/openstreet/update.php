<?php namespace Larasite\Http\Requests\openstreet;

use Larasite\Http\Requests\Request;
use Larasite\Library\LibReg as REG;

class update extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return false;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{	

		$lib = new REG;
		$reg = $lib->get();
		return [
			"data"=> [
				'id'=>'required|numeric',
				'no_ruas'=>'required|alpha_num|min:4|max:30',
				'id_kecamatan'=>'required|numeric',
				'name'=>'required|'.$reg['name'].'|min:4|max:30',
				'patok_sta'=>'required|'.$reg['sta'].'|min:4|max:10',
				'geo_dms'=>'required|string|min:6|max:30',
				'geo_decimal'=>'required|'.$reg['latlon'].'|min:6|max:30',
				'panjang_jalan'=>'required|numeric',
				'lebar_jalan'=>'required|numeric',
				'deitil_permukaan'=>[
					'susunan'=>'numeric|nullable',
					'kondisi'=>'numeric|nullable',
					'penurunan'=>'string|nullable',
					'tambalan'=>'string|nullable',
				],
				'detil_kerusakan'=>[
					'detil_keretakan'=>[
						'jenis'=>'numeric|nullable',
						'lebar'=>'numeric|nullable',
						'luas'=>'string|nullable',
					],
					'detil_kerusakan_lain'=>[
						'jumlah_lubang'=>'string|nullable',
						'ukuran_lubang'=>'string|nullable',
						'bekas_roda'=>'string|nullable',
						'kerusakan_tepi_kiri'=>'string|nullable',
						'kerusakan_tepi_kanan'=>'string|nullable',
					],
					'detil_bahu_jalan'=>[
						'bahu_jalan_kiri'=>'string|nullable',
						'bahu_jalan_kanan'=>'string|nullable',
						'permukaan_bahu_kiri'=>'string|nullable',
						'permukaan_bahu_kanan'=>'string|nullable',
						'kondisi_saluran_kiri'=>'string|nullable',
						'kondisi_saluran_kanan'=>'string|nullable',
						'lereng_kiri'=>'string|nullable',
						'lereng_kanan'=>'string|nullable',
						'trotoar_kiri'=>'string|nullable',
						'trotoar_kanan'=>'string|nullable',
					]
				],
				'kondisi_ruas'=>'required|numeric',
				'penanganan_ruas'=>'required|numeric',
				'jenis_kekerasan'=>'required|numeric'
			],
			"type"=> "object"
		];
	}

}
