<?php namespace Larasite\Http\Middleware\jwt;

use Closure;
use JWTAuth;
Use Larasite\UserTest;
class verify {

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{	
		try {
			$token = explode("Bearer ",$request->header('authorization'))[1];
			$model = new UserTest;
			$get = $model->where("remember_token",'=',$token)->get(["id"]);
			if(count($get)){
				return $next($request);
			}
			return \Response::json([
				"data"=> null,
				"code"=>401,
				"message"=>"Access Denied",
				"type"=>"object"
			],401);
		} catch (\Throwable $th) {
			return \Response::json([
				"data"=> null,
				"code"=>401,
				"message"=>"Access Denied",
				"type"=>"object"
			],401);
		}
		return $next($request);
	}

}
