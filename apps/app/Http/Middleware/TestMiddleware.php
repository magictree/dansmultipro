<?php namespace Larasite\Http\Middleware;

use Closure;

class TestMiddleware {

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */

	private function set_valid()
	{
		$reg = ['text_num'=>'Regex:/^[ñA-Za-z0-9\-! ,\'\"\/@\.:\(\)]+$/',
			'text'=>'Regex:/^[ñA-Za-z\-! ,\'\"\/@\.:\(\)]+$/',
			'num'=>'Regex:/^[0-9-\^ ]+$/',
			'hmo'=>'Regex:/^[ñA-Za-z0-9-\^ ]+$/',
			'twit'=>'regex:/^[ñA-Za-z0-9_]{1,15}$/'];
		
		$rule = [
				// 'first_name'=>'alpha',
				// 'middle_name'=>'alpha',
				// 'last_name'=>$reg['text'],
				// 'ad_username'=>$reg['text_num'],
				// 'sss'=>$reg['num'],
				// 'philhealth'=>$reg['num'],
				// 'hdmf'=>$reg['num'],
				// 'maiden_name'=>'alpha',
				// 'hmo_account'=>$reg['hmo'],
				// 'hmo_numb'=>$reg['hmo'],
				// 'rtn'=>$reg['num'],
				// 'mid'=>$reg['num'],
				// 'tin'=>$reg['num'],
				// 'gender'=>'alpha',
				// 'marital_status'=>'alpha',
				// 'nationality'=>'numeric',				
				// 'date_of_birth'=>'date',
				// 'place_of_birth'=>'alpha',
				// 'religion'=>'alpha',
				// 'biometric'=>'min:2'
				'first_name'=>$reg['text_num'],
				'middle_name'=>$reg['text_num'],
				'last_name'=>$reg['text_num'],
				'ad_username'=>$reg['text_num'],
				'sss'=>$reg['text_num'],
				'philhealth'=>$reg['num'],
				'hdmf'=>$reg['text_num'],
				'hmo_account'=>$reg['hmo'],
				'hmo_numb'=>$reg['hmo'],
				'rtn'=>$reg['text_num'],
				'mid'=>$reg['text_num'],
				'tin'=>$reg['text_num'],
				'gender'=>$reg['text_num'],
				'marital_status'=>$reg['text_num'],
				'nationality'=>$reg['text_num'],
				'date_of_birth'=>'date',
				'place_of_birth'=>$reg['text_num'],
				'religion'=>$reg['text_num']
		];
		if($rule['gender'] == 'female' && $rule['marital_status'] != 'single' ){ 
			$rule['maiden_name'] = "required|".$reg['text_num']; 
		}
		$valid = \Validator::make(\Input::all(),$rule); return $valid;
	}

	public function handle($request, Closure $next)
	{
		//return \Response::json(200);
		//$last_segment = count($request->segments());
		//return \Response::json($request->segment($last_segment));
		//if(gettype($request->segments($last_segment-1)))

		//return \Response::json($request->all());
		// ALLOW SPECIAL CHAR
		if(null != $request->get('data')){
			
			// NO JSON
			if(gettype($request->get('data')) != 'array'){			
				$inputs = json_decode($request->get('data'),1);
				
				if(isset($inputs['comment'])){			

					$inputs['comment'] = addslashes($inputs['comment']);
					$request->merge(['data'=>json_encode($inputs)]);
				}
			}
			/*$inputs = json_decode($request->get('data'),1);
			
			if(isset($inputs['comment'])){			

				$inputs['comment'] = addslashes($inputs['comment']);
				$request->merge(['data'=>json_encode($inputs)]);
			}*/
			

		}else{

			if(null != $request->get('comment')){
				if(gettype($request->get('comment')) != 'array'){
					$comments = $request->get('comment');

					$allow_special = addslashes($comments);
					
					$request->merge(['comment'=>$allow_special]);
				}
			}

			if(null != $request->get('newcomment')){
				if(gettype($request->get('newcomment')) != 'array'){
					$newcomments = $request->get('newcomment');

					$allow_special = addslashes($newcomments);
					
					$request->merge(['newcomment'=>$allow_special]);
				}
			}			

			
		}

		//return \Response::json('test');
		/*if(null != $request->route()->parameters('id')){

			$rule = [ 'id' => 'required|numeric', 'filename'=>"Regex:/^[ñA-Za-z.]+$/" ];
			
			$valid = \Validator::make($request->all() ,$rule); 
			
			if($valid->fails()){
				return \Response::json('Fails');	
			}else{
				return \Response::json("OK Valid");
			}
		}*/
		return $next($request);
	}

}
