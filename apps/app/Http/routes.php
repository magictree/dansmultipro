<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.

*/

use Illuminate\Cookie\CookieJar;
use Larasite\Http\Requests;
use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

Route::group([
	"prefix"=>"apidev",	

], function(){
	Route::group([
		"prefix"=>"/users",	
		"middleware"=>"jwt.auth"
	], function(){
		Route::get("/","user_manage@index");
		Route::post("/show","user_manage@show");
		Route::post("/create","user_manage@create");
		Route::post("/update","user_manage@update");
		Route::post("/actived_account","user_manage@actived_account");

	});

	Route::group([
		"prefix"=>"/auth",	
	], function(){
		Route::post("/login","jwt_auth@login");
		Route::get("/logout","jwt_auth@logout");
		Route::post("/registered","jwt_auth@registered");
		Route::post("/activation_user","jwt_auth@activation");

	});

	Route::group([
		"prefix"=>"/job_api",	
		"middleware"=>"jwt.auth"
	], function(){
		Route::get("/","job_api@index");
		Route::post("/show","job_api@show");
	});
});

Route::get('/',function(){
	return 100;
});

