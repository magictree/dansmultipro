<?php namespace Larasite\Http\Controllers;

use Larasite\Http\Requests;
use Larasite\Http\Controllers\Controller;
use Larasite\Model\jalan;
use Larasite\Model\detil_konstruksi_jalan;
use Illuminate\Http\Request;
use Larasite\Http\Requests\openstreet\create as request_create;
class openstreet_ctrl extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */

	protected $result = ["data"=>null,"message"=>null,"code"=>200,"type"=>"object"];
	public function index()
	{
		// view default data
		$models = new jalan;
		$con = $models->get_list_jalan(["id"=>0]);
		
		$this->result['meesage'] = $con->message;
		$this->result['code'] = $con->code;
		//return \Response::json($con,$this->result['code']);

		//$get = jalan::where("active","=",1)->get();
		$panjang_jalan = 0;

		for($i=0; $i<count($con->data); $i++){
			$panjang_jalan += $con->data[$i]->panjang_jalan;
		}
		$this->result['data'] = ['list'=>$con->data,'chart'=>['panjang_jalan'=>$panjang_jalan,'total_jalan'=>count($con->data)]];
		$this->result['type'] = 'object';

		return \Response::json($this->result,$this->result['code']);
	}

	public function get_list_kabupaten()
	{
		$model = new jalan;

		$res = $model->get_kabupaten();
		$this->result['message']='View List';
		$this->result['data'] = $res;
		$this->result['type'] = 'object';

		return \Response::json($this->result,200);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{	
		$request_create  = new request_create;
		$defaults = $request_create->default_value_detail();

		$model = new jalan;
		//$new_street = \Request::all();
		//$rules = $request_create->rules();
		
		// $valid = \Validator::make($new_street, $rules);
		// if($valid->fails()){
		// 	$this->result['message'] = "Data isn't correnct, please check again.";
		// 	$this->code = 400;
		//}else{

		function csvToArray($filename = '', $delimiter = ',')
		{
			if (!file_exists($filename) || !is_readable($filename))
				return false;

			$header = null;
			$data = array();
			if (($handle = fopen($filename, 'r')) !== false)
			{
				while (($row = fgetcsv($handle, 1000, $delimiter)) !== false)
				{
					if (!$header)
						$header = $row;
					else
						$data[] = array_combine($header, $row);
				}
				fclose($handle);
			}

			return $data;
		}
		$data = csvToArray(storage_path('kotalama.csv'));

		for ($i=0; $i < count($data); $i++) { 
			$new_street = [
				"data"=>[
				"no_ruas"=>$data[$i]['no_ruas'],
				"nama_ruas"=>$data[$i]['nama_ruas'],
				"id_kecamatan"=>5371041,
				"patok_sta"=>$data[$i]['patok_sta'],
				"geo_dms"=>"null",
				"geo_decimal"=>"null",
				"lebar_jalan"=>$data[$i]['lebar_jalan'],
				"panjang_jalan"=>$data[$i]['panjang_jalan']
			]
			];
		
			if(!isset($new_street['data']['detil_kerusakan'])){
				$new_street['data']['detil_kerusakan'] = $defaults['detil_kerusakan'];
				$new_street['data']['detil_kerusakan'] = json_encode($new_street['data']['detil_kerusakan']);
			}else{
				$new_street['data']['detil_kerusakan'] = json_encode($new_street['data']['detil_kerusakan']);
			}
	
			if(!isset($new_street['data']['detil_permukaan'])){
				$new_street['data']['detil_permukaan'] = $defaults['detil_permukaan'];
				$new_street['data']['detil_permukaan'] = json_encode($new_street['data']['detil_permukaan']);
			}else{
				$new_street['data']['detil_permukaan'] = json_encode($new_street['data']['detil_permukaan']);
			}
			
			if(!isset($new_street['data']['detil_bahu_jalan'])){
				$new_street['data']['detil_bahu_jalan'] = $defaults['detil_bahu_jalan'];
				$new_street['data']['detil_bahu_jalan'] = json_encode($new_street['data']['detil_bahu_jalan']);
			}else{
				$new_street['data']['detil_bahu_jalan'] = json_encode($new_street['data']['detil_bahu_jalan']);
			}
	
			if(!isset($new_street['data']['kondisi_ruas']))
				$new_street['data']['kondisi_ruas'] = $defaults['kondisi_ruas'];
	
			if(!isset($new_street['data']['penanganan_ruas']))
				$new_street['data']['penanganan_ruas'] = $defaults['penanganan_ruas'];
	
			if(!isset($new_street['data']['jenis_kekerasan']))
				$new_street['data']['jenis_kekerasan'] = $defaults['jenis_kekerasan'];
	
			if(!isset($new_street['data']['klasifikasi_status']))
				$new_street['data']['klasifikasi_status'] = $defaults['klasifikasi_status'];
	
			if(!isset($new_street['data']['klasifikasi_fungsi']))
				$new_street['data']['klasifikasi_fungsi'] = $defaults['klasifikasi_fungsi'];
	
			$insert_jalan = $model->store($new_street['data']);
			$this->result['message'] = $insert_jalan->message;
			$this->result['code'] = $insert_jalan->code;
		}
		//}

		
		return \Response::json($this->result, $this->result['code']);
	}


	/**
	 * Display the specified resource.
	 *
	 * @return Response
	 */
	public function show(Request $req)
	{
		$sub = $req->segments()[0];
		$model = new jalan;
		$street = \Request::all();
		
		$valid = \Validator::make(
			$street, 
			[
				'data.id'=>'required|numeric|max:10]',
				'type'=>'required|in:numeric'
			]);
		
		
		if($valid->fails()){
			$this->result['message'] = "Data isn't correct, please check your input.";
			$this->result['code'] = 400;
		}else{

			$show_jalan = $model->show($street['data']['id']);
			
			$this->result['message'] = $show_jalan->message;
			$this->result['code'] = $show_jalan->code;
			$this->result['data'] = $show_jalan->data;
		}

		if($sub == 'public_alpha'){
			unset($this->result['data']->detil_kerusakan);
			unset($this->result['data']->detil_permukaan);
			unset($this->result['data']->detil_bahu_jalan);
			
		}

		return \Response::json($this->result, $this->result['code']);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @return Response
	 */
	public function edit()
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update()
	{	
		$request_create  = new request_create;
		$defaults = $request_create->default_value_detail();

		$model = new jalan;
		$new_street = \Request::all();
		$rules = $request_create->rules();

		$rules['data.id'] = "required|numeric";
		
		$valid = \Validator::make($new_street, $rules);
		if($valid->fails()){
			$this->result['message'] = "Data isn't correnct, please check again.";
			$this->code = 400;
		}else{

			if(!isset($new_street['data']['detil_kerusakan'])){
				$new_street['data']['detil_kerusakan'] = $defaults['detil_kerusakan'];
				$new_street['data']['detil_kerusakan'] = json_encode($new_street['data']['detil_kerusakan']);
			}else{
				$new_street['data']['detil_kerusakan'] = json_encode($new_street['data']['detil_kerusakan']);
			}
	
			if(!isset($new_street['data']['detil_permukaan'])){
				$new_street['data']['detil_permukaan'] = $defaults['detil_permukaan'];
				$new_street['data']['detil_permukaan'] = json_encode($new_street['data']['detil_permukaan']);
			}else{
				$new_street['data']['detil_permukaan'] = json_encode($new_street['data']['detil_permukaan']);
			}
			
			if(!isset($new_street['data']['detil_bahu_jalan'])){
				$new_street['data']['detil_bahu_jalan'] = $defaults['detil_bahu_jalan'];
				$new_street['data']['detil_bahu_jalan'] = json_encode($new_street['data']['detil_bahu_jalan']);
			}else{
				$new_street['data']['detil_bahu_jalan'] = json_encode($new_street['data']['detil_bahu_jalan']);
			}
	
			if(!isset($new_street['data']['kondisi_ruas']))
				$new_street['data']['kondisi_ruas'] = $defaults['kondisi_ruas'];
	
			if(!isset($new_street['data']['penanganan_ruas']))
				$new_street['data']['penanganan_ruas'] = $defaults['penanganan_ruas'];
	
			if(!isset($new_street['data']['jenis_kekerasan']))
				$new_street['data']['jenis_kekerasan'] = $defaults['jenis_kekerasan'];
	
			if(!isset($new_street['data']['klasifikasi_status']))
				$new_street['data']['klasifikasi_status'] = $defaults['klasifikasi_status'];
	
			if(!isset($new_street['data']['klasifikasi_fungsi']))
				$new_street['data']['klasifikasi_fungsi'] = $defaults['klasifikasi_fungsi'];
			

			// update
			$update_jalan = $model->update_jalan($new_street['data']);
			
			if(!$update_jalan->error){

				$this->result['message'] = $update_jalan->message;
			}else{

				$this->result['message'] = "Insert failed.";
				$this->result['code'] = 400;
			}

		}

		
		return \Response::json($this->result, $this->result['code']);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy()
	{
		$models = new jalan;
		$this->result['type'] = 'object';
		$dataPayload = \Request::all();
		$rules = [
			"data.id"=>"required|numeric",
		];
		$validator = \Validator::make($dataPayload, $rules);
		if($validator->fails()){
			$this->result['message'] = "Input isn't correct, please check again.";
			$this->result['code'] = 400;
		}else{
			$con = $models->non_active_jalan($dataPayload['data']);			
			$this->result['message'] = $con->message;
			$this->result['code'] = $con->code;
		}
		
		return \Response::json($this->result, $this->result['code']);
	}

}
