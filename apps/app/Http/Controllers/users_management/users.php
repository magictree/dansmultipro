<?php namespace Larasite\Http\Controllers\users_management;

use Larasite\Http\Requests;
use Larasite\Http\Controllers\Controller;
use Larasite\UserTest;
use Illuminate\Http\Request;


class users extends Controller {
	
	Protected $result = ["data"=>null,"message"=>null,"type"=>null,"code"=>200];
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$get = Usertest::all();
		$this->result['data'] = $get;
		$this->result['message'] = 'View user list';
		$this->result['type'] = 'array';
		return \Response::json($this->result, $this->result['code']);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show()
	{
		$data = \Input::get('data');
		$fails = false;
		foreach($data as $key => $val){
			if(gettype($val) != 'integer'){
				$fails = true;
			}
		}
		if($fails){
			$this->result['message'] = "Data isn't correct, Please check your input.";
			$this->result['code'] = 400;
		}else{

			$get = Usertest::find(\Input::get('data'));
			$this->result['data'] = $get;
			$this->result['message'] = 'View user';
		}
		$this->result['type'] = 'array';
		return \Response::json($this->result, $this->result['code']);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update()
	{
		$reg = \LibRegex::get_instance();
		$newuser = \Input::get('data');

		$this->result['type'] = "object";
		$rules = [
			"id"=>"required|numeric",
			"username"=> "required|".$reg['name']."|min:8|max:30",
			"email"=> "required|email",
			"phone"=> $reg['phone']
		];
		$valid = \Validator::make($newuser,$rules);
		if($valid->fails()){
			$this->result['message'] = "Data isn't correct, Please check your input";
		}else{
			$id = $newuser['id'];
			$user = UserTest::find($newuser['id']);
			
			unset($newuser['id']);

			foreach($newuser as $key =>$value){
				$user[$key] = $value;
			}
			$user->save();
			$this->result['message'] = 'Update Success.';
		}
		
		return \Response::json($this->result,200);
	}

	public function update_password()
	{
		$reg = \LibRegex::get_instance();
		$newuser = \Input::get('data');

		$this->result['type'] = "object";
		$rules = [
			"id"=>"required|numeric",
			"current_password"=> "required|".$reg['name']."|min:8|max:30",
			"new_password"=> "required|".$reg['name']."|min:8|max:30",
			"confirm_password"=> "required|".$reg['name']."|min:8|max:30"
		];
		$valid = \Validator::make($newuser,$rules);
		if($valid->fails()){
			$this->result['message'] = "Input isn't correct, please check again.";
			$this->result['code'] = 400;
		}else{
			$id = $newuser['id'];
			$user = UserTest::find($newuser['id']);
			try {
				//code...
				$match = \Hash::check($newuser['current_password'], $user['password']);
				if($match){
	
					$pass = \Hash::make($newuser['new_password']);
					$user->password = $pass;
					$user->save();
					$this->result['message'] = 'Update Success.';
				}else{
					$this->result['message'] = 'Password not valid.';
					$this->result['code'] = 400;
				}
			} catch (\Exception $th) {
				$this->result['message'] = $th->getMessage();
				$this->result['code'] = 400;
			}
		}
		
		return \Response::json($this->result,$this->result['code']);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy()
	{
		$models = new UserTest;
		$this->result['type'] = 'object';
		$dataPayload = \Request::all();
		$rules = [
			"data.id"=>"required|numeric",
		];
		$validator = \Validator::make($dataPayload, $rules);
		if($validator->fails()){
			$this->result['message'] = "Input isn't correct, please check again.";
			$this->result['code'] = 400;
		}else{
			$con = $models->non_active_users($dataPayload['data']);			
			$this->result['message'] = $con->message;
			$this->result['code'] = $con->code;
		}
		
		return \Response::json($this->result, $this->result['code']);
	}

}
