<?php namespace Larasite\Http\Controllers;

use Larasite\Http\Requests;
use Larasite\Http\Controllers\Controller;

use JWTAuth;
use JWTFactory;
use Tymon\JWTAuth\Exceptions\JWTException;
use Larasite\accounts;

use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;

use Illuminate\Http\RedirectResponse as redirect;

class jwt_auth extends Controller {

	protected $result = ["data"=>null, "message"=>null, "type"=>null, "code"=>200];

	public function logout(Request $request)
	{
		$models = new accounts;
		$this->result['type'] = 'object';
		try {
			//code...
			$head = explode("Bearer ",$request->header('Authorization'))[1];
			$decode = JWTAuth::decode_url($head);
			$decode['token'] = $head;
			
			$con = $models->logout_account($decode);		
			$this->result['message'] = $con->message;
			$this->result['type'] = 'object';
			$this->result['code'] = $con->code;
			
			return \Response::json($this->result,$this->result['code']);
		} catch (\Exception $th) {
			return response()->json(['data'=>null, 'message'=>$th->getMessage(), 'type'=>'object'], 500);
		}
	}

	public function login(Request $request)
	{	
		$models = new accounts;
		$credentials = $request->only('username', 'password');
		$this->result['type']="object";

		$valid = \Validator::make($credentials,[
			"username"=>"required|string|min:8",
			"password" => "required|string|min:8"
		]);

		if($valid->fails()){
			$this->result['message'] = "Incorrect username or password.";
			$this->result['type'] = 'object';
			$this->result['code'] = 400;
			return response()->json($this->result, $this->result['code']);
		}
		
		try {
			$find = accounts::where("username","=",$credentials['username'])->get(["username","email","phone"]);
			
			if(count($find) == 0){
				$this->result['message'] = "invalid credential";
				$this->result['type'] = 'object';
				$this->result['code'] = 400;
			}else{				
				if (!$token = JWTAuth::attempt($credentials,["username"=>$find[0]->username,"email"=>$find[0]->email,"phone"=>$find[0]->phone])) {
					$this->result['data'] = $token;
					$this->result['message'] = "Incorrect username or password.";
					$this->result['code'] = 400;
				}else{

					\DB::select("update accounts set remember_token = ? where username = ?",[$token, $find[0]->username]);
					$this->result['data'] = ['token'=>$token,"user"=>$find[0]];
					$this->result['message'] = 'SUCCESS.';
					$this->result['code'] = 200;
				}
				
			}

			return \Response::json($this->result,$this->result['code']);

        } catch (JWTException $e) {
			$this->result['message'] = "could_not_create_token, please contact your admin";
			$this->result['code'] = 500;
            return response()->json($this->result, $this->result['code']);
        }
	}

	public function authorization()
	{
		try {
			if (!$user = JWTAuth::parseToken()->authenticate() ) {
				return \Response::json(['User not found'],$e->getStatusCode());
			}
		} catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
			return \Response::json($e);
		} catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {
			return \Response::json(['Token Invalid'],$e->getStatusCode());
		} catch (Tymon\JWTAuth\Exceptions\JWTException $e) {
			return \Response::json(['Token Absent'],$e->getStatusCode());
		}

		$token = JWTAuth::getToken();
		$user = JWTAuth::toUser($token);
 
		return \Response::json([
			'data' => [
				'detail' => $user,
				'registered_at' => $user->created_at->toDateTimeString()
			]
		]);
	}

	public function registered(Request $request)
	{	
		$models = new accounts;
		$reg = \LibRegex::get_instance();
		$newuser = $request->all();
		$this->result['type'] = "object";

		$newuser['password'] = $request->input('password');
		$rules = [
			"username"=> "required|".$reg['name']."|min:8|max:30",
			"email"=> "required|email",
			"phone"=> "$reg[phone]|min:11",
			"password"=>"required|".$reg['name']."|min:8|max:30"
		];
		
		$valid = \Validator::make($newuser,$rules);
		if($valid->fails()){

			$this->result['message'] = "Data isn't correct, please check your input again.";
			$this->result['code'] = 400;
		}else{
			$newuser['password'] = \Hash::make($newuser['password']);

			$make_token = JWTFactory::make($newuser);
			$token = JWTAuth::encode($make_token);
			$newuser['remember_token'] = $token;

			$con = $models->register_account($newuser);
			
			$this->result['message'] = $con->message;
			$this->result['code'] = $con->code;
			if(!$con->error){

				$url = \Config::get('app.url');
				$url = "$url/auth/activation_user/$token";
				$data = array('name'=>$newuser['username'],"request" => 'activation', 'email' => $newuser['email'], 'url_activation'=>$url);
		
				if($data['email']){
					\Mail::send('emails.activation_user', $data, function($message) use ($data){
						$message->to($data['email'])->subject("Activation user");
					});
				}else{
					$this->result['message'] = "Email incorrect.";
					$this->result['code'] = 400;
				}
			}
		
		}
		
		return \Response::json($this->result,$this->result['code']);
	}

	// $code is param url
	
	public function activation($code)
	{
		$decode = \JWTAuth::decode_url($code);
		$models = new accounts;

		$object = [
			"username" =>$decode['username'],
			"email" => $decode['email'],
			"token"=> $code
		];
		$con = $models->activation($object);
		return [$con];
		$this->result['message'] = $con->message;
		$this->result['code'] = $con->code;
		return \Response::json($this->result, $this->result['code']);
	
	}

}
