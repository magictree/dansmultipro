<?php namespace Larasite\Http\Controllers;

use Larasite\Http\Requests;
use Larasite\Http\Controllers\Controller;
use Larasite\accounts;

use Illuminate\Http\Request;

class user_manage extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */

	protected $result = ["data"=>null,"message"=>null, "type"=>'object', "code"=>200];
	public function index()
	{
		$model = new accounts;
		$con = $model->get_list_users(["id"=>0]);
		$this->result['message'] = $con->message;
		$this->result['data'] = $con->data;
		$this->result['code'] = $con->code;

		return \Response::json($this->result, $con->code);
	}	

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$reg = \LibRegex::get_instance();
		$model = new accounts;
		$payload = \Request::all();

		$valid = \Validator::make($payload, [
			"username"=>"required|".$reg['name']."|min:3|max:30",
			"email"=>"required|email",
			"password"=>"required|".$reg['name']."|min:8|max:30",
			"phone"=>"required|".$reg['phone']."|min:11"
		]);

		if($valid->fails()){
			$this->result['message'] = "Data isn't correct, Please check your input again.";
			$this->result['code'] = 400;			
		}else{

			$payload['password'] = \Hash::make($payload['password']);
			
			$con = $model->create_account($payload);
			
			$this->result['message'] = $con->message;
			$this->result['code'] = $con->code;
		}

		return \Response::json($this->result, $this->result['code']);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show()
	{
		$model = new accounts;
		$payload = \Request::all();

		$valid = \Validator::make($payload, [
			"id"=>"required|numeric"
		]);

		if($valid->fails()){
			$this->result['message'] = "Data isn't correct, Please check your input again.";
			$this->result['code'] = 400;			
		}else{

			$con = $model->get_list_users($payload);
			$this->result['message'] = $con->message;
			$this->result['data'] = $con->data;
			$this->result['code'] = $con->code;
		}

		return \Response::json($this->result, $this->result['code']);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update()
	{
		$reg = \LibRegex::get_instance();
		$model = new accounts;
		$payload = \Request::all();
		$valid = \Validator::make($payload, [
			"id"=>"required|numeric",
			"username"=>"required|".$reg['name']."|min:3|max:30",
			"email"=>"required|email",
			"phone"=>"required|".$reg['phone']."|min:11"
		]);

		if($valid->fails()){
			$this->result['message'] = "Data isn't correct, Please check your input again.";
			$this->result['code'] = 400;			
		}else{

			$con = $model->update_account($payload);

			$this->result['message'] = $con->message;
			$this->result['code'] = $con->code;
		}

		return \Response::json($this->result, $this->result['code']);
	}

	public function non_active_account()
	{
		$reg = \LibRegex::get_instance();
		$model = new accounts;
		$payload = \Request::input("data");
		$valid = \Validator::make($payload, [
			"id"=>"required|numeric"
		]);

		if($valid->fails()){
			$this->result['message'] = "Data isn't correct, Please check your input again.";
			$this->result['code'] = 400;			
		}else{
			$payload['type'] = '0';
			$con = $model->actived_account($payload);

			$this->result['message'] = $con->message;
			$this->result['code'] = $con->code;
		}

		return \Response::json($this->result, $this->result['code']);
	}

	public function actived_account()
	{
		$reg = \LibRegex::get_instance();
		$model = new accounts;
		$payload = \Request::all();
		$valid = \Validator::make($payload, [
			"id"=>"required|numeric",
			"type"=> "required|numeric|in:0,1"
		]);

		if($valid->fails()){
			$this->result['message'] = "Data isn't correct, Please check your input again.";
			$this->result['code'] = 400;			
		}else{

			$con = $model->actived_account($payload);

			$this->result['message'] = $con->message;
			$this->result['code'] = $con->code;
		}

		return \Response::json($this->result, $this->result['code']);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
