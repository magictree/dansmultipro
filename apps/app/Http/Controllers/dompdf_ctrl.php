<?php namespace Larasite\Http\Controllers;

// use Mail;
// use App\User;
use Larasite\Http\Requests;
use Larasite\Http\Controllers\Controller;
use Larasite\Library\FuncAccess;

class dompdf_ctrl extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public $restfull = true;
	

	private function slip_incentive($data){
		$tmp = '<!DOCTYPE html>
		<html>


		<head>

		</head>

		<style>
		table {
			border-collapse: collapse;
		}

		table, th, td {
			border: 1px solid black;
		}
		</style>

		<body style="padding: 20px;border-style: solid">
		<div>
		<table>
		<thead>
		<tr>
		<td><img src="http://10.129.1.12:8080/get-images/company/General_informationRyL5i5.jpg" alt="company"/></td>
		<td><h1>Incentive Slips</h1></td>

		</tr>
		</thead>
		<tbody>
		<tr >
		<td>Employee Name</td>

		<td>'.$data['employee_name'].'</td>

		</tr>
		<tr>
		<td>Periode Covered</td>

		<td>'.$data['periode_covered'].'</td>

		</tr>
		<tr>
		<td>Monthly Incentive</td>

		<td>'.$data['monthly_incentive'].'</td>

		</tr>
		<tr>
		<td>Food Allowance</td>

		<td>'.$data['food_allowance'].'</td>

		</tr>
		<tr>
		<td>ISO Allowance</td>

		<td>'.$data['iso_allowance'].'</td>

		</tr>
		<tr>
		<td>Dealer Incentive</td>

		<td>'.$data['dealer_incentive'].'</td>

		</tr>
		<tr>
		<td>Team Leader Incentive</td>

		<td>'.$data['team_leader_incentive'].'</td>

		</tr>
		<tr>
		<td>Absences</td>

		<td>'.$data['abs'].'</td>

		</tr>
		<tr>
		<td>Tardiness</td>

		<td>'.$data['tar'].'</td>

		</tr>
		<tr>
		<td>Disciplinary Action</td>

		<td>'.$data['disciplin'].'</td>

		</tr>
		<tr>
		<td>Adjustment</td>

		<td>'.$data['adjustment_payroll'].'</td>

		</tr>
		<tr>
		<td><b>Gross Pay</b></td>

		<td><b>'.$data['total'].'</b></td>

		</tr>
		<tr>
		<td>Deduction Monthly Incentive</td>

		<td>'.$data['deductions_monthly'].'</td>

		</tr>
		<tr>
		<td>Deduction Food Allowance</td>

		<td>'.$data['deductions_food'].'</td>

		</tr>
		<tr>
		<td>Deduction ISO Allowance</td>

		<td>'.$data['deductions_iso_allow'].'</td>

		</tr>
		<tr>
		<td>Deduction Dealer Incentive</td>

		<td>'.$data['deductions_dealer'].'</td>

		</tr>
		<tr>
		<td>Deduction Team Leader Incentive</td>

		<td>'.$data['deductions_team_leader'].'</td>

		</tr>
		<tr>
		<td><b>Net Pay</b></td>

		<td><b>'.$data['total'].'</b></td>

		</tr> 

		<tr>
		<td><b>Net Pay</b></td>

		<td><b>'.$data['total'].'</b></td>

		</tr> 
		<tr>
		<td><b>Received By</b></td>

		<td><b>Date</b></td>

		</tr> 
		<tr>
		<td><br><br><br></td>

		<td><br><br><br></td>

		</tr> 



		</tbody>
		</table>
		</div>



		</body>
		</html>';
		return $tmp;
	}

	public function slips_monthly($data){
		$tmp = '

		<html>


		<head>
		<title>Slips Salary</title>
		</head>

		<style>
		table {
			border-collapse: collapse;
		}

		table,
		th,
		td {
			/*		border: 1px solid black;*/
			padding: 4px 4px 4px 4px;
			/*vertical-align: top;*/
		}
		td.custom{
			border: 0;
			padding: 3px 0 3px 0;
		}
		</style>

		<body>
		<div style="border-style: solid; padding:10px">
		<table style="width: 100%;font-size:11px">
		<tr>
		<td style="width: 50%;font-size:11px">

		<img src="http://10.129.1.12:8080/get-images/company/General_informationRyL5i5.jpg" alt="company"/>
		</td>
		<td style="width: 50%;font-size:11px;">
		<div style="font-size:11px;text-align: right">
		8th Floor Techzone<br>
		213 Senator Gil Puyat Avenue<br>
		City of Makati,Metro Manila<br>
		Philippines,1203<br>
		</div>

		</td>	
		</tr>

		</table>
		<table style="width: 100%;font-size:11px">
		<tr>
		<td style="width: 100%;font-size:11px;background-color:#c8c8c8">
		<center >

		<h1><u>PAYROLL SLIP</u></h1>


		</center>
		</td>

		</tr>

		</table>

		<table style="width:100%; border: 0;font-size:11px">
		</table>



		<table style="width: 100%;font-size:11px;border:1px solid black;margin-bottom: 5px;margin-top: 5px">

		<tbody>
		<tr>
		<td><b>Employee Number </b> <b style="padding-left:9px">: '.$data['employee_id'].'</b></td>

		<td><b>Employee Name </b>  <b style="padding-left:14px">: '.$data['employee_name'].'</b></td>

		</tr>
		<tr>
		<td><b>Department </b><b style="padding-left:43px">: '.$data['department_name'].'</b></td>

		<td><b>Designation  </b><b style="padding-left:38px">: '.$data['job_title'].'</b></td>
		</tr>
		<tr>
		<td><b>Payout Date  </b><b style="padding-left:42px">: '.$data['payout_date'].'</b></td>

		<td><b>Attendance Period  </b> <b style="padding-left:4px">: '.$data['payroll_period'].'</b></td>

		</tr>

		</tbody>
		</table>


		<table style="width:100%; border: 0;font-size:11px">
		<thead>
		<tr >
		<td style="background-color:#c8c8c8;border-top: 1px solid;border-left: 1px solid;"><b>Income</b></td>
		<td style="text-align: right;background-color:#c8c8c8;border-top: 1px solid;border-right: 1px solid"><b>Amount</b></td>
		<td></td>

		<td style="background-color:#c8c8c8;border-top: 1px solid;border-left: 1px solid;"><b>Government Deduction</b></td>
		<td style="text-align: right;background-color:#c8c8c8;border-top: 1px solid;border-right: 1px solid;"><b>Amount</b></td>
		<td></td>

		<td style="background-color:#c8c8c8;border-top: 1px solid;border-left: 1px solid;"><b>Employee Share</b></td>
		<td style="background-color:#c8c8c8;border-top: 1px solid"></td>
		<td style="text-align: right;background-color:#c8c8c8;border-top: 1px solid;border-right: 1px solid;"><b>Amount</b></td>




		</tr>
		</thead>
		<tbody>
		<tr>
		<td style="border-left: 1px solid;">Basic Salary</td>
		<td style="text-align: right;border-right: 1px solid;">'.$data['basic_salary'].'</td>
		<td></td>


		<td style="border-left: 1px solid;">SSS Contribution</td>
		<td style="text-align: right;border-right: 1px solid;">'.$data['sss_cont'].'</td>
		<td></td>

		<td style="border-left: 1px solid;">SSS Contribution</td>
		<td></td>
		<td style="text-align: right;border-right: 1px solid;">'.$data['sss_cont'].'</td>

		</tr>
		<tr>
		<td style="border-left: 1px solid;">Ecola</td> 
		<td style="text-align: right;border-right: 1px solid;">'.$data['ecola_salary'].'</td>
		<td></td>

		<td style="border-left: 1px solid;">Philhealth Contribution</td> 
		<td style="text-align: right;border-right: 1px solid;">'.$data['philhealth_cont'].'</td>
		<td></td>



		<td style="border-left: 1px solid;">Philhealth Contribution</td> 
		<td></td>
		<td style="text-align: right;border-right: 1px solid;">'.$data['philhealth_cont'].'</td>				


		</tr>

		<tr>
		<td style="border-left: 1px solid;">Overtime</td>
		<td style="text-align: right;border-right: 1px solid;">'.$data['overtime'].'</td>
		<td></td>

		<td style="border-left: 1px solid;">HDMF Contribution</td>
		<td style="text-align: right;border-right: 1px solid;">'.$data['hdmf_cont'].'</td>
		<td></td>

		<td style="border-left: 1px solid;border-bottom:1px solid">HDMF Contribution</td>
		<td style="border-bottom:1px solid"></td>
		<td style="text-align: right;border-right: 1px solid;border-bottom:1px solid">'.$data['hdmf_cont'].'</td>

		</tr>

		<tr>
		<td style="border-left: 1px solid;">Night Differential</td>
		<td style="text-align: right;border-right: 1px solid;">'.$data['night_differential'].'</td>
		<td></td>

		<td style="border-left: 1px solid;">HDMF 2 Contribution</td>
		<td style="text-align: right;border-right: 1px solid;">'.$data['hdmf_cont'].'</td>
		<td></td>


		<td></td>
		<td></td>
		<td></td>

		</tr>

		<tr>
		<td style ="border-left: 1px solid;">Leave With Pay</td>
		<td style="text-align: right;border-right: 1px solid;">'.$data['leave_with_pay'].'</td>
		<td></td>

		<td style="border-left: 1px solid;">Withholding Tax</td>
		<td style="text-align: right;border-right: 1px solid;">'.$data['withholding_tax'].'</td>
		<td></td>


		<td></td>
		<td></td>
		<td></td>

		</tr>

		<tr>
		<td style="border-left: 1px solid;">Holiday Work Pay</td>
		<td style="text-align: right;border-right: 1px solid;">'.$data['total_holiday_pay'].'</td>
		<td></td>

		<td style="background-color:#c8c8c8;border-left: 1px solid;"><b>Government Loan</b></td>
		<td style="text-align: right;background-color:#c8c8c8;border-right: 1px solid;"><b>Amount</b></td>
		<td></td>


		<td></td>
		<td></td>
		<td></td>

		</tr>

		<tr>
		<td style="border-left: 1px solid;">Absences</td>
		<td style="text-align: right;border-right: 1px solid;">'.$data['absences'].'</td>
		<td></td>

		<td style="border-left: 1px solid;">SSS Loan</td>
		<td style="text-align: right;border-right: 1px solid;">'.$data['sss_loan'].'</td>
		<td></td>



		<td></td>
		<td></td>
		<td></td>

		</tr>

		<tr>
		<td style="border-left: 1px solid;">Tardiness</td>
		<td style="text-align: right;border-right: 1px solid;">'.$data['tardiness'].'</td>
		<td></td>

		<td style="border-left: 1px solid;">HDMF Loan</td>
		<td style="text-align: right;border-right: 1px solid;">'.$data['hdmf_loan'].'</td>
		<td></td>

		<td></td>
		<td></td>
		<td></td>

		</tr>

		<tr>
		<td style="border-left: 1px solid;">Adjustment</td>
		<td style="text-align: right;border-right: 1px solid;">'.$data['adjusments'].'</td>
		<td></td>

		<td style="border-left: 1px solid;">SSS Calamity Loan</td>
		<td style="text-align: right;border-right: 1px solid;">'.$data['sss_calamity_loan'].'</td>
		<td></td>

		<td></td>
		<td></td>
		<td></td>

		</tr>


		<tr>
		<td style="border-left: 1px solid;"></td>
		<td style="text-align: right;border-right: 1px solid;"></td>
		<td></td>

		<td style="border-left: 1px solid;">HDMF Calamity Loan</td>
		<td style="text-align: right;border-right: 1px solid;">'.$data['hdmf_calamity_loan'].'</td>
		<td></td>

		<td></td>
		<td></td>
		<td></td>

		</tr>

		<tr>
		<td style="border-left: 1px solid;"></td>
		<td style="text-align: right;border-right:1px solid;"></td>
		<td></td>

		<td style="background-color:#c8c8c8;border-left: 1px solid;"><b>Other Deduction</b></td>
		<td style="text-align: right;background-color:#c8c8c8;border-right: 1px solid;"><b>Amount</b></td>
		<td></td>


		<td></td>
		<td></td>
		<td></td>

		</tr>

		<tr>
		<td style="border-left: 1px solid;"></td>
		<td style="text-align: right;border-right: 1px solid;"></td>
		<td></td>

		<td style="border-left: 1px solid;">HMO</td>
		<td style="text-align: right;border-right: 1px solid;">'.$data['hmo'].'</td>
		<td></td>

		<td></td>
		<td></td>
		<td></td>

		</tr>

		<tr>
		<td style="border-left: 1px solid;"></td>
		<td style="text-align: right;border-right: 1px solid;"></td>
		<td></td>

		<td style="border-left: 1px solid;">Others</td>
		<td style="text-align: right;border-right: 1px solid;">'.$data['other'].'</td>
		<td></td>


		<td></td>
		<td></td>
		<td></td>

		</tr>
		<tr>
		<td style="background-color:#c8c8c8;border-left: 1px solid;border-bottom: 1px solid;"><b>Total Income</b></td>
		<td style="text-align: right;background-color:#c8c8c8;border-right: 1px solid;border-bottom: 1px solid;"><b>'.$data['gross_pay'].'</b></td>
		<td></td>

		<td style="background-color:#c8c8c8;border-left: 1px solid;border-bottom: 1px solid;"><b>Total Deduction</b></td>
		<td style="text-align: right;background-color:#c8c8c8;border-right: 1px solid;border-bottom: 1px solid;"><b>'.$data['total_deduction'].'</b></td>
		<td></td>


		<td style="background-color:#c8c8c8;border-left: 1px solid;border-bottom: 1px solid;border-top:1px solid"><b>Net Income</b></td>
		<td style="background-color:#c8c8c8;border-bottom: 1px solid;border-top:1px solid"></td>
		<td style="text-align: right;background-color:#c8c8c8;border-right: 1px solid;border-bottom: 1px solid;border-top:1px solid"><b>'.$data['net_pay'].'</b></td>

		</tr>


		</tbody>

		</table>
		<center style="font-size:9px"><i>***This is a computer generated Payslip and requires no signature***</i></center>






		</div>
		</body>

		</html>


		';
		return $tmp;
	}

	public function slips_bonus($data){
		$tmp = '<!DOCTYPE html>
		<html>


		<head>

		</head>

		<style>
		table {
			border-collapse: collapse;
		}

		table, th, td {
			border: 1px solid black;
		}
		</style>

		<body style="padding: 20px; border-style: solid">
		<div>
		<table>
		<thead>
		<tr>
		<td><img src="http://10.129.1.12:8080/get-images/company/General_informationRyL5i5.jpg" alt="company"/></td>
		<td><h1>Bonus Slip</h1></td>

		</tr>
		</thead>
		<tbody>
		<tr >
		<td>Employee Name</td>

		<td>'.$data['employee_name'].'</td>

		</tr>
		<tr>
		<td>Periode Covered</td>

		<td>'.$data['periode_covered'].'</td>

		</tr>
		<tr>
		<td>13th Month Pay</td>

		<td>'.$data['pay13th'].'</td>

		</tr>

		<tr>
		<td>Net Performance Bonus</td>

		<td>'.$data['net_performance_bonus'].'</td>

		</tr>
		<tr>
		<td>Management Bonus</td>

		<td>'.$data['management_bonus'].'</td>

		</tr>
		<tr>
		<td>Others</td>

		<td>'.$data['total_holiday_pay'].'</td>

		</tr>
		<tr>
		<td><b>Gross Pay</b></td>

		<td><b>'.$data['gross_pay'].'</b></td>

		</tr>
		<tr>
		<td colspan="2"><b>Deductions</b></td>

		<!-- <td>'.$data['absences'].'</td> -->

		</tr>
		<tr>
		<td>Withholding Tax</td>

		<td>'.$data['tardiness'].'</td>

		</tr>
		<tr>
		<td>HMO</td>

		<td>'.$data['adjusments'].'</td>

		</tr>
		<tr>
		<td><b>Others Deduction</b></td>

		<td><b>'.$data['gross_pay'].'</b></td>

		</tr>
		<tr>
		<td><b>Total Deduction</b></td>

		<td><b>'.$data['withholding_tax'].'</b></td>

		</tr>
		<tr>
		<td><b>Net Pay</b></td>

		<td><b>'.$data['sss_cont'].'</b></td>

		</tr>


		<tr>
		<td><b>Received By</b></td>

		<td><b>Date</b></td>

		</tr> 
		<tr>
		<td><br><br><br></td>

		<td><br><br><br></td>

		</tr> 



		</tbody>
		</table>
		</div>



		</body>
		</html>';
		return $tmp;
	}

	public function slips_sil($data){
		$tmp = '<!DOCTYPE html>
		<html>


		<head>

		</head>

		<style>
		table {
			border-collapse: collapse;
		}

		table, th, td {
			border: 1px solid black;
		}
		</style>

		<body style="padding: 20px; border-style: solid">
		<div>
		<table>
		<thead>
		<tr>
		<td><img src="http://10.129.1.12:8080/get-images/company/General_informationRyL5i5.jpg" alt="company"/></td>
		<td><h1>Bonus Slip</h1></td>

		</tr>
		</thead>
		<tbody>
		<tr >
		<td>Employee Name</td>

		<td>'.$data['employee_name'].'</td>

		</tr>
		<tr>
		<td>Periode Covered</td>

		<td>'.$data['periode_covered'].'</td>

		</tr>
		<tr>
		<td>13th Month Pay</td>

		<td>'.$data['pay13th'].'</td>

		</tr>

		<tr>
		<td>Net Performance Bonus</td>

		<td>'.$data['net_performance_bonus'].'</td>

		</tr>
		<tr>
		<td>Management Bonus</td>

		<td>'.$data['management_bonus'].'</td>

		</tr>
		<tr>
		<td>Others</td>

		<td>'.$data['total_holiday_pay'].'</td>

		</tr>
		<tr>
		<td><b>Gross Pay</b></td>

		<td><b>'.$data['gross_pay'].'</b></td>

		</tr>
		<tr>
		<td colspan="2"><b>Deductions</b></td>

		<!-- <td>'.$data['absences'].'</td> -->

		</tr>
		<tr>
		<td>Withholding Tax</td>

		<td>'.$data['tardiness'].'</td>

		</tr>
		<tr>
		<td>HMO</td>

		<td>'.$data['adjusments'].'</td>

		</tr>
		<tr>
		<td><b>Others Deduction</b></td>

		<td><b>'.$data['gross_pay'].'</b></td>

		</tr>
		<tr>
		<td><b>Total Deduction</b></td>

		<td><b>'.$data['withholding_tax'].'</b></td>

		</tr>
		<tr>
		<td><b>Net Pay</b></td>

		<td><b>'.$data['sss_cont'].'</b></td>

		</tr>


		<tr>
		<td><b>Received By</b></td>

		<td><b>Date</b></td>

		</tr> 
		<tr>
		<td><br><br><br></td>

		<td><br><br><br></td>

		</tr> 



		</tbody>
		</table>
		</div>



		</body>
		</html>';
		return $tmp;
	}
	

	
	public function generate()
	{	
		
		def("DOMPDF_ENABLE_REMOTE", false);
		$storages 	= storage_path("dompdf");
		// $storages = "";
		$input = \Input::all();
		if(isset($input['data'])){
			$input = $input['data'];
		}
		//return gettype($input);

		$date 				= date("Y-m-d");
		$time 				= time();

		$arr = [];
		if(gettype($input) == 'array'){
			foreach ($input as $key1 => $value1) {
				try{

					if($value1['type_slip'] == "incentive_slip"){
						$template 			= $this->slip_incentive($value1);
					}if($value1['type_slip'] == "monthly_slip"){
						$template 			= $this->slips_monthly($value1);
					}if($value1['type_slip'] == "bonus_slip"){
						$template 			= $this->slips_bonus($value1);
					}if($value1['type_slip'] == "sil_slips"){
						$template 			= $this->slips_sil($value1);
					}
					$period 			= str_replace(", ","_",$value1['periode_covered']);
					$filename 			= $value1['type_slip']."_".$period."_".$value1['employee_name']."_".$date."_".$time.".pdf";
					$path 				= $storages."/slip_monthly";
				}catch(\Exception $e){
					return [$e,$key1,$value1];
				}
				// $path = $storages;
				$save 				= $path."/".$filename;

				$pdf = \PDF::loadHTML($template)->setPaper('a4','portrait')->setWarnings(false);
				$pdf->save($save, $value1['employee_id']);
				array_push($arr,["employee_id"=>$value1['employee_id'],'name_file'=>$filename,'path'=>$path,"dirfull"=>$save]);
			}
		}else{

			if($input['type_slip'] == "incentive_slip"){
				$template 			= $this->slip_incentive($input);
			}if($input['type_slip'] == "monthly_slip"){
				$template 			= $this->slips_monthly($input);
			}if($input['type_slip'] == "bonus_slip"){
				$template 			= $this->slips_bonus($input);
			}if($input['type_slip'] == "sil_slips"){
				$template 			= $this->slips_sil($input);
			}
			$period 			= str_replace(", ","_",$input['periode_covered']);
			$filename 			= $input['type_slip']."_".$period."_".$input['employee_name']."_".$date."_".$time.".pdf";
			$path 				= $storages."/slip_monthly";
			// $path = $storages;
			$save 				= $path."/".$filename;

			$customPaper = array(0,0,360,360);
			// $pdf = \PDF::loadHTML($template)->setPaper('a4','portrait')->setWarnings(false)->save($save);
			$pdf = \PDF::loadHTML($template)->setPaper($customPaper,'portrait')->setWarnings(false);
			$pdf->save($save, $input['employee_id']);
			array_push($arr,[ "employee_id"=>$input['employee_id'],'name_file'=>$filename,'path'=>$path,"dirfull"=>$save]);
		}

		// header("Cache-Control: public");
		// header("Content-Description: File Transfer");
		// header("Content-Disposition: attachment; filename=$save");
		// header("Content-Type: application/pdf");
		// header("Content-Transfer-Encoding: binary");
		// return readfile($save);




		return \Response::json(['header'=>['message'=>"Success Generate!",'status'=>200, "access" => true],'data'=>$arr],200);

		// return response()->download($save);

	}

	public function generate_download()
	{	
		
		def("DOMPDF_ENABLE_REMOTE", false);
		$storages 	= storage_path("dompdf");
		// $storages = "";
		$input = \Input::all();
		if(isset($input['data'])){
			$input = $input['data'];
		}
		//return gettype($input);

		$date 				= date("Y-m-d");
		$time 				= time();

		$arr = [];
		if(gettype($input) == 'array'){
			foreach ($input as $key1 => $value1) {
				try{

					if($value1['type_slip'] == "incentive_slip"){
						$template 			= $this->slip_incentive($value1);
					}if($value1['type_slip'] == "monthly_slip"){
						$template 			= $this->slips_monthly($value1);
					}if($value1['type_slip'] == "bonus_slip"){
						$template 			= $this->slips_bonus($value1);
					}if($value1['type_slip'] == "sil_slips"){
						$template 			= $this->slips_sil($value1);
					}
					$period 			= str_replace(", ","_",$value1['periode_covered']);
					$filename 			= $value1['type_slip']."_".$period."_".$value1['employee_name']."_".$date."_".$time.".pdf";
					$path 				= $storages."/slip_monthly";
				}catch(\Exception $e){
					// dd("test");
					return [$e,$key1,$value1];
				}
				// $path = $storages;
				$save 				= $path."/".$filename;

				$pdf = \PDF::loadHTML($template)->setPaper('a4','portrait')->setWarnings(false);
				$pdf->save($save, $value1['employee_id']);
				array_push($arr,["employee_id"=>$value1['employee_id'],'name_file'=>$filename,'path'=>$path,"dirfull"=>$save]);
			}
		}else{

			if($input['type_slip'] == "incentive_slip"){
				$template 			= $this->slip_incentive($input);
			}if($input['type_slip'] == "monthly_slip"){
				$template 			= $this->slips_monthly($input);
			}if($input['type_slip'] == "bonus_slip"){
				$template 			= $this->slips_bonus($input);
			}if($input['type_slip'] == "sil_slips"){
				$template 			= $this->slips_sil($input);
			}
			$period 			= str_replace(", ","_",$input['periode_covered']);
			$filename 			= $input['type_slip']."_".$period."_".$input['employee_name']."_".$date."_".$time.".pdf";
			$path 				= $storages."/slip_monthly";
			// $path = $storages;
			$save 				= $path."/".$filename;

			$customPaper = array(0,0,360,360);
			// $pdf = \PDF::loadHTML($template)->setPaper('a4','portrait')->setWarnings(false)->save($save);
			$pdf = \PDF::loadHTML($template)->setPaper($customPaper,'portrait')->setWarnings(false);
			$pdf->save($save, $input['employee_id']);
			array_push($arr,[ "employee_id"=>$input['employee_id'],'name_file'=>$filename,'path'=>$path,"dirfull"=>$save]);
		}

		// header("Cache-Control: public");
		// header("Content-Description: File Transfer");
		// header("Content-Disposition: attachment; filename=$save");
		// header("Content-Type: application/pdf");
		// header("Content-Transfer-Encoding: binary");
		// return readfile($save);



		$type = \File::mimeType($path);
		return \Response::json(['header'=>['message'=>"Success!",'status'=>200, "access" => true],'data'=>"/dompdf/slip_monthly/".$filename],200);
		// return \Response::download($path,'example.xlsx',['Content-Type'=>$type]);
		
		$path = storage_path("/dompdf/slip_monthly/".$filename);
		$file = \File::get($path);
		$type = \File::mimeType($path);
		return \Response::make($file,200,['Content-Type'=>$type]); 	
		//return response()->download($save,$filename,['Content-Type'=>$type]);

	}

	public function send_email_pdf(){

		$input = \Input::all();

		if(isset($input['data'])){

			foreach ($input['data'] as $key => $value) {

				$payout_date = $value['payout_date'];
				$name_file = $value['name_file'];
				$employee_id = $value['employee_id'];
				$set_data  =  \DB::SELECT("CALL `email__GET`('$employee_id')");
				if(count($set_data) > 0){
					if(isset($set_data[0]->work_email)){
						$set_email = $set_data[0]->work_email;
					}else if(isset($set_data[0]->personal_email)){
						$set_email = $set_data[0]->personal_email;
					}


					$work_email = $set_data[0]->work_email;
					$personal_email = $set_data[0]->personal_email;
					$fullname = $set_data[0]->fullname;



					$payout_date_split = explode("-",$payout_date);
					// return $payout_date_split;
					$payout_date_split  = mktime(0, 0, 0, $payout_date_split[1]  , $payout_date_split[2], $payout_date_split[0]);
					// return $payout_date;

					$payout_date = date("F j, Y",$payout_date_split); 
					$data = array('name'=>$fullname,'email_work' => $work_email, 'email_personal'=>$personal_email, 'name_file' =>$name_file, 'payout_date' =>$payout_date);


					/*\Mail::raw('test', function($message) use ($data){
						// $req = $data['request'];
						$message->to($data['email_work'])->subject("test");
						$storages 	= storage_path("dompdf");
						$path = $storages."/slip_monthly";
						$message->attach($path."/".$data['name_file']);
					});

					\Mail::raw('test', function($message) use ($data){
						// $req = $data['request'];
						$message->to($data['email_personal'])->subject("test");
						$storages 	= storage_path("dompdf");
						$path = $storages."/slip_monthly";
						$message->attach($path."/".$data['name_file']);;
					});*/




					if(isset($data['email_work'])){
						\Mail::raw($payout_date . ' Payroll Slip', function($message) use ($data){
							// $req = $data['request'];

							$message->to($data['email_work'])->subject($data['payout_date'] . ' Payroll Slip');
							$storages 	= storage_path("dompdf");
							$path = $storages."/slip_monthly";
							$message->attach($path."/".$data['name_file']);
						});
					}else{
						if(isset($data['email_personal'])){
							\Mail::raw($payout_date . ' Payroll Slip', function($message) use ($data){
								// $req = $data['request'];
								$message->to($data['email_personal'])->subject($data['payout_date'] . ' Payroll Slip');
								$storages 	= storage_path("dompdf");
								$path = $storages."/slip_monthly";
								$message->attach($path."/".$data['name_file']);;
							});
						}
					}
					//return response()->json(['header' => ['message' =>  'success send file to email',  'status' => 200] ,'data' => []], 200);
					

					// $namex  =  $input['employee'];
					// $request =  $input['type'];
					// $email   =  $set_data;
				}else{
					//return response()->json(['header' => ['message' =>  'success send email',  'status' => 200] ,'data' => []], 200);
				}
			}
		}else{

			$name_file = $input['name_file'];
			$employee_id = $input['employee_id'];
			$set_data  =  \DB::SELECT("CALL `email__GET`('$employee_id')");
			if(count($set_data) > 0){
				if(isset($set_data[0]->work_email)){
					$set_email = $set_data[0]->work_email;
				}else if(isset($set_data[0]->personal_email)){
					$set_email = $set_data[0]->personal_email;
				}


				$work_email = $set_data[0]->work_email;
				$personal_email = $set_data[0]->personal_email;
				$fullname = $set_data[0]->fullname;
				$payout_date_split = explode("-",$payout_date);
					// return $payout_date_split;
				$payout_date_split  = mktime(0, 0, 0, $payout_date_split[1]  , $payout_date_split[2], $payout_date_split[0]);
					// return $payout_date;

				$payout_date = date("F j, Y",$payout_date_split); 
				$data = array('name'=>$fullname,'email_work' => $work_email, 'email_personal'=>$personal_email, 'name_file' =>$name_file, 'payout_date' =>$payout_date);


				// $data = array('name'=>$fullname,'email_work' => $work_email, 'email_personal'=>$personal_email, 'name_file' =>$name_file);


				if(isset($data['email_work'])){
					\Mail::raw($payout_date . ' Payroll Slip', function($message) use ($data){
						// $req = $data['request'];
						$message->to($data['email_work'])->subject($payout_date . ' Payroll Slip');
						$storages 	= storage_path("dompdf");
						$path = $storages."/slip_monthly";
						$message->attach($path."/".$data['name_file']);
					});
				}else{
					if(isset($data['email_personal'])){
						\Mail::raw($payout_date . ' Payroll Slip', function($message) use ($data){
							// $req = $data['request'];
							$message->to($data['email_personal'])->subject($payout_date . ' Payroll Slip');
							$storages 	= storage_path("dompdf");
							$path = $storages."/slip_monthly";
							$message->attach($path."/".$data['name_file']);;
						});
					}
				}
				return response()->json(['header' => ['message' =>  'success send file to email',  'status' => 200] ,'data' => []], 200);
				

				// $namex  =  $input['employee'];
				// $request =  $input['type'];
				// $email   =  $set_data;
			}else{
				return response()->json(['header' => ['message' =>  'success send email',  'status' => 200] ,'data' => []], 200);
			}
		}
		return response()->json(['header' => ['message' =>  'success send email',  'status' => 200] ,'data' => []], 200);
		// return response()->json([$name_file,$employee_id],200);


	}

}
