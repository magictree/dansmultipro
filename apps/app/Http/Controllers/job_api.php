<?php namespace Larasite\Http\Controllers;

use Larasite\Http\Requests;
use Larasite\Http\Controllers\Controller;

use Illuminate\Http\Request;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

class job_api extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	protected $result = ["data"=>null,"message"=>null, "type"=>'object', "code"=>200];
	protected $apis ="https://jobs.github.com/positions";
	protected $json_object = null;
	
	public function __construct(){
		$http = new Client();
		$req = $http->createRequest('GET', $this->apis.".json");
		$res = $http->send($req);

		if($res->getStatusCode() == 200){
			$json = $res->json();
			if(!$json)
				$json = $res->getBody();

			$this->json_object = $json;
		}
	}

	public function index(Request $request){
		$reg = \LibRegex::get_instance();
		$payload = \Request::only(["description","location","full_time"]);
		$valid = \Validator::make($payload,[
			"description"=> $reg['name']."|min:3|max:50",
			"location"=> $reg['name']."|min:3|max:50",
			"full_time"=> "string|in:on"
		]);

		if($valid->fails()){
			$this->result['message'] = "Data isn't correct, Please check your input again.";
			$this->result['code'] = 400;			
		}else{

			$filter = [];
			if($payload['full_time']){
				for($i=0; $i < count($this->json_object); $i++){
					if($this->json_object[$i]['type'] == 'Full Time'){
						$filter[] = $this->json_object[$i];
					}
				}
			}else{
				$filter = $this->json_object;
			}

			
			$filter1 = [];
			if($payload['description']){

				for($i=0; $i < count($filter); $i++){
					$cek = strpos(strtolower($filter[$i]['description']), strtolower($payload['description']));
					if(gettype($cek) == 'integer'){
						$filter1[] = $filter[$i];
					}
				}
			}else{
				$filter1[] = $filter;
			}


			$filter2 = [];
			if($payload['location']){

				for($i=0; $i < count($filter1); $i++){
					
					$cek = strpos(strtolower($filter1[$i]['location']), strtolower($payload['location']));
					if(gettype($cek) == 'integer'){
						$filter2[] = $filter1[$i];
					}
				}
			}else{
				$filter2[] = $filter1;
			}

			// $collections = collect($filter2);
			// $page = $request->query('page') || 1;
			// $product = $collections->slice(20,$page)->all();

			$this->result['data'] = $filter2;
			$this->result['code'] = 200;
			$this->result['message'] = 'Success';
			if(count($filter2) == 0)
				$this->result['message'] = 'Data Not Found.';

			// foreach($payload as $key => $value){
			
			// 	if($value){
			// 		if(strlen($att) == 0){
			// 			$att .= "?$key=$value";
			// 		}else{
			// 			$att .= "&$key=$value";
			// 		}
			// 	}
			// }

			// $urls = $this->apis."".$att;
			// $filter = [];
			
			// $http = new Client();
			// $req = $http->createRequest('GET', "https://jobs.github.com/positions.json");
			// $res = $http->send($req);

			// if($res->getStatusCode() != 200){
			// 	$this->result['data'] = null;
			// 	$this->result['code'] = $res->getStatusCode();
			// 	$this->result['message'] = 'Failed';
			// }else{
			// 	$json = $res->json();
			// 	if(!$json){
			// 		$json = $res->getBody();
			// 	}
			// 	$this->result['data'] = $json;
			// 	$this->result['code'] = $res->getStatusCode();
			// 	$this->result['message'] = 'Success';
			// }
		}
		return \Response::json($this->result,$this->result['code']);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show()
	{
		$reg = \LibRegex::get_instance();
		$payload = \Request::all();
		$valid = \Validator::make($payload,[
			"id"=> "required|".$reg['name']
		]);

		if($valid->fails()){
			$this->result['message'] = "Data isn't correct, Please check your input again.";
			$this->result['code'] = 400;			
		}else{

			$http = new Client();
			$urls = $this->apis."/".$payload['id'].".json";
			$req = $http->createRequest('GET', $urls);
			$res = $http->send($req);

			$this->result['code'] = $res->getStatusCode();
			if($res->getStatusCode() != 200){
				$this->result['message'] = 'Failed';
			}else{
				$json = $res->json();
				if(!$json){
					$json = $res->getBody();
				}
				$this->result['data'] = $json;
				$this->result['code'] = $res->getStatusCode();
				$this->result['message'] = 'Success';
			}
		}
		return \Response::json($this->result,$this->result['code']);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
