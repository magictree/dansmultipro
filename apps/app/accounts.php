<?php namespace Larasite;

use Illuminate\Database\Eloquent\Model;
use Larasite\Library\FuncDB;
use Illuminate\Database\QueryException;
class accounts extends Model {

	Protected $procedures = ["get_list_account","create_account","update_account","non_active_account","actived_account"];
	Protected $procedures_auth = ["user_login","user_logout","register_user","cek_activation_users"];
	Protected $tables = 'accounts';
	Protected $hiddden = ['id',"password"];
	Protected $fillable = [];
	protected $result = ["error"=>false,"data"=>null,"message"=>null,"code"=>200];

	public function getJWTIdentifier()
    {
        return $this->getKey();
    }
    public function getJWTCustomClaims()
    {
        return [];
    }

	public function login_account($data){
		$funcdb = new FuncDB;
		
		$params = $funcdb->get_param_procedure($this->procedures_auth[0], $data);
		
		try {
			$result = \DB::select("CALL ".$this->procedures_auth[0]."(".$params['lenght'].")",$params['col']); // FULLL INPUT
			
			$this->result['message'] = $result[0]->message;
			$this->result['code'] = $result[0]->code;
			return (object)$this->result;
		
		} catch (QueryException $th) {
			$msg = $th->errorInfo;
			if(isset($th->errorInfo[2])){
				$msg = $th->errorInfo[2];
			}
			$this->result['message'] = $msg;
			$this->result['error'] = true;
			$this->result['code'] = 500;
			return (object)$this->result;
		}
	}

	public function logout_account($data){
		$funcdb = new FuncDB;
		
		$params = $funcdb->get_param_procedure($this->procedures_auth[1], $data);
		try {
			$result = \DB::select("CALL ".$this->procedures_auth[1]."(".$params['lenght'].")",$params['col']); // FULLL INPUT
			
			$this->result['message'] = $result[0]->message;
			$this->result['code'] = $result[0]->code;
			return (object)$this->result;
		
		} catch (QueryException $th) {
			$msg = $th->errorInfo;
			if(isset($th->errorInfo[2])){
				$msg = $th->errorInfo[2];
			}
			$this->result['message'] = $msg;
			$this->result['error'] = true;
			$this->result['code'] = 500;
		}	
	}

	public function get_list_users($data){
		$funcdb = new FuncDB;
		
		$params = $funcdb->get_param_procedure($this->procedures[0], $data);
		//return $params;
		try {
			$result = \DB::select("CALL ".$this->procedures[0]."(".$params['lenght'].")",$params['col']); // FULLL INPUT
				
			if(count($result) > 0){

				$this->result['message'] = "SUCCESS";
			}else{
				$this->result['message'] = "Data is empty";
			}
			$this->result['data'] = $result;
			
			return (object)$this->result;
		
		} catch (QueryException $th) {
			$msg = $th->errorInfo;
			if(isset($th->errorInfo[2])){
				$msg = $th->errorInfo[2];
			}
			$this->result['message'] = $msg;
			$this->result['error'] = true;
			$this->result['code'] = 500;
			return (object)$this->result;
		}
	}

	public function register_account($data){
		$funcdb = new FuncDB;
		
		$params = $funcdb->get_param_procedure($this->procedures_auth[2], $data);
		
		try {
			$result = \DB::select("CALL ".$this->procedures_auth[2]."(".$params['lenght'].")",$params['col']); // FULLL INPUT
			
			$this->result['message'] = $result[0]->message;
			$this->result['code'] = $result[0]->code;
			return (object)$this->result;
		
		} catch (QueryException $th) {
			$msg = $th->errorInfo;
			if(isset($th->errorInfo[2])){
				$msg = $th->errorInfo[2];
			}
			$this->result['message'] = $msg;
			$this->result['error'] = true;
			$this->result['code'] = 500;
			return (object)$this->result;
		}
	}

	public function create_account($data){
		$funcdb = new FuncDB;
		
		$params = $funcdb->get_param_procedure($this->procedures[1], $data);
		
		try {
			$result = \DB::select("CALL ".$this->procedures[1]."(".$params['lenght'].")",$params['col']); // FULLL INPUT
			
			$this->result['message'] = $result[0]->message;
			$this->result['code'] = $result[0]->code;
			return (object)$this->result;
		
		} catch (QueryException $th) {
			$msg = $th->errorInfo;
			if(isset($th->errorInfo[2])){
				$msg = $th->errorInfo[2];
			}
			$this->result['message'] = $msg;
			$this->result['error'] = true;
			$this->result['code'] = 500;
			return (object)$this->result;
		}
	}

	public function update_account($data){
		$funcdb = new FuncDB;
		
		$params = $funcdb->get_param_procedure($this->procedures[2], $data);
		try {
			$result = \DB::select("CALL ".$this->procedures[2]."(".$params['lenght'].")",$params['col']); // FULLL INPUT
			
			$this->result['message'] = $result[0]->message;
			$this->result['code'] = $result[0]->code;
			return (object)$this->result;
		
		} catch (QueryException $th) {
			$msg = $th->errorInfo;
			if(isset($th->errorInfo[2])){
				$msg = $th->errorInfo[2];
			}
			$this->result['message'] = $msg;
			$this->result['error'] = true;
			$this->result['code'] = 500;
			return (object)$this->result;
		}
	}

	public function non_active_account($data){
		$funcdb = new FuncDB;
		
		$params = $funcdb->get_param_procedure($this->procedures[3], $data);
		try {
			$result = \DB::select("CALL ".$this->procedures[3]."(".$params['lenght'].")",$params['col']); // FULLL INPUT
			
			$this->result['message'] = $result[0]->message;
			$this->result['code'] = $result[0]->code;
			return (object)$this->result;
		
		} catch (QueryException $th) {
			$msg = $th->errorInfo;
			if(isset($th->errorInfo[2])){
				$msg = $th->errorInfo[2];
			}
			$this->result['message'] = $msg;
			$this->result['error'] = true;
			$this->result['code'] = 500;
			return (object)$this->result;
		}
	}

	public function actived_account($data){
		$funcdb = new FuncDB;
		
		$params = $funcdb->get_param_procedure($this->procedures[4], $data);
		try {
			$result = \DB::select("CALL ".$this->procedures[4]."(".$params['lenght'].")",$params['col']); // FULLL INPUT
			
			$this->result['message'] = $result[0]->message;
			$this->result['code'] = $result[0]->code;
			return (object)$this->result;
		
		} catch (QueryException $th) {
			$msg = $th->errorInfo;
			if(isset($th->errorInfo[2])){
				$msg = $th->errorInfo[2];
			}
			$this->result['message'] = $msg;
			$this->result['error'] = true;
			$this->result['code'] = 500;
			return (object)$this->result;
		}
	}

	public function activation($data){
		$funcdb = new FuncDB;
		
		$params = $funcdb->get_param_procedure($this->procedures_auth[3], $data);
		
		try {
			$result = \DB::select("CALL ".$this->procedures_auth[3]."(".$params['lenght'].")",$params['col']); // FULLL INPUT
			
			$this->result['message'] = $result[0]->message;
			$this->result['code'] = $result[0]->code;
			return (object)$this->result;
		
		} catch (QueryException $th) {
			$msg = $th->errorInfo;
			if(isset($th->errorInfo[2])){
				$msg = $th->errorInfo[2];
			}
			$this->result['message'] = $msg;
			$this->result['error'] = true;
			$this->result['code'] = 500;
			return (object)$this->result;
		}
	}

}
