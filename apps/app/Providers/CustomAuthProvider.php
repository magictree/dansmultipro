<?php namespace Larasite\Providers;

use Larasite\Auth_Model;
use Larasite\Auth\CustomUserProvider;
use Illuminate\Support\ServiceProvider;

class CustomAuthProvider extends ServiceProvider {


	public function boot()
	{
		$this->app['auth']->extend('custom',function(){
			return new CustomUserProvider(new User);
		});
	}

	/**
	 * Register the application services.
	 *
	 * @return void
	 */
	public function register()
	{
		//
	}

}
