<?php namespace Larasite\Model;

use Illuminate\Database\Eloquent\Model;
use Larasite\Model\detil_openstreet_model;
use Larasite\Model\wilayah;
use Larasite\Library\FuncDB;
use Illuminate\Database\QueryException;

class jalan extends Model {

	//
	Protected $procedures = ['insert_jalan_full_ux','show_jalan','update_jalan','delete_jalan','showall_jalan',"get_list_jalan",'update_detil_jalan',"non_active_jalan"];
	Protected $tables = 'jalans';
	//Protected $fillable = ['no_ruas','nama_ruas','id_kecamatan','patok_sta','geo_dms','geo_decimal','panjang_jalan','lebar_jalan','galeri'];
	Protected $hiddden = ['id'];
	Protected $fillable = [];
	protected $result = ["error"=>false,"data"=>null,"message"=>null,"code"=>200];
	
	public function __construct(){
		$funcdb = new FuncDB;
		$res  = $funcdb->get_columns($this->tables);
		$this->fillable = $res;
	}

	public function get_list_jalan($data){
		$funcdb = new FuncDB;
		
		$params = $funcdb->get_param_procedure($this->procedures[5], $data);
		try {
			$result = \DB::select("CALL ".$this->procedures[5]."(".$params['lenght'].")",$params['col']); // FULLL INPUT
			
			$this->result['data'] = $result;
			$this->result['message'] = "SUCCESS";
			
			return (object)$this->result;
		
		} catch (QueryException $th) {
			$msg = $th->errorInfo;
			if(isset($th->errorInfo[2])){
				$msg = $th->errorInfo[2];
			}
			$this->result['message'] = $msg;
			$this->result['error'] = true;
			$this->result['code'] = 500;
			return (object)$this->result;
		}
	}

	public function store($data){

		try {
			$funcdb = new FuncDB;
			
			$params = $funcdb->get_param_procedure($this->procedures[0], $data);
			
			$result = \DB::SELECT("CALL ".$this->procedures[0]."(".$params['lenght'].")",$params['col'])[0]; // FULLL INPUT
			
			$this->result['message'] = $result->message;
			$this->result['code'] = $result->code;
			if($result->code == 200){
				$this->error = true;
			}
		} catch (QueryException $th) {
			$msg = $th->errorInfo;
			if(isset($th->errorInfo[2])){
				$msg = $th->errorInfo[2];
			}
			$this->result['message'] = $msg;
			$this->result['error'] = true;
			$this->result['code'] = 500;
		}

		return (object)$this->result;
	}

	public function update_jalan($data){
		$funcdb = new FuncDB;
		try {
			$params = $funcdb->get_param_procedure($this->procedures[2], $data);
			$params_detil = $funcdb->get_param_procedure($this->procedures[6], $data);
			
			if(count($params_detil) > 0){
				\DB::beginTransaction();

				$result_jalan = \DB::SELECT("CALL ".$this->procedures[2]."(".$params['lenght'].")",$params['col'])[0]; // FULLL INPUT
				
				if($result_jalan->code == 200){

					$result_detil = \DB::SELECT("CALL ".$this->procedures[6]."(".$params_detil['lenght'].")",$params_detil['col'])[0]; // FULLL INPUT
					if($result_detil->code == 200){
						\DB::commit();
						$this->result['message'] = $result_detil->message;
					}else{
						\DB::rollback();	
						return 1;
						$this->result['message'] = $result_detil['message'];
						$this->result['error'] = true;
						$this->result['code'] = 500;
					}
				}else{
					\DB::rollback();
					$this->result['message'] = $result_jalan->message;
					$this->result['error'] = true;
					$this->result['code'] = 500;
				}
			}else{
				$result = \DB::SELECT("CALL ".$this->procedures[2]."(".$params['lenght'].")",$params['col']); // FULLL INPUT
				if($result_detil->code == 200){
					\DB::commit();
					$this->result['message'] = $result_detil->message;
				}else{
					\DB::rollback();	
					$this->result['message'] = $result_detil->message;
					$this->result['error'] = true;
					$this->result['code'] = 500;
				}
			}
			
		
		} catch (QueryException $th) {
			\DB::rollback();
			$this->result['message'] = $th->errorInfo;
			$this->result['error'] = true;
			$this->result['code'] = 500;
		}

		return (object)$this->result;
	}

	public function non_active_jalan($data){
		$funcdb = new FuncDB;
		
		$params = $funcdb->get_param_procedure($this->procedures[7], $data);
		try {
			$result = \DB::SELECT("CALL ".$this->procedures[7]."(".$params['lenght'].")",$params['col']); // FULLL INPUT
			
			$this->result['message'] = $result[0]->message;
			$this->result['code'] = $result[0]->code;
			if($result[0]->code == 200){
				$this->result['error'] = true;
			}
			
			return (object)$this->result;
		
		} catch (QueryException $th) {
			$msg = $th->errorInfo;
			if(isset($th->errorInfo[2])){
				$msg = $th->errorInfo[2];
			}
			$this->result['message'] = $msg;
			$this->result['error'] = true;
			$this->result['code'] = 500;
			return (object)$this->result;
		}
	}
	public function show($data){
		$funcdb = new FuncDB;

		//$data = implode(",",$data);
		// $res = $funcdb->get_param_procedure($this->procedures[1]);
		// $params = [];

		// for($i=0; $i < count($res); $i++){
		// 	$alias = $res[$i];
		// 	if(isset($data[$alias]))
		// 		$params[] = $data[$alias];
		//}

		$params = $funcdb->get_param_procedure($this->procedures[5], ["id"=>$data]);
		$result = \DB::SELECT("CALL ".$this->procedures[5]."(".$params['lenght'].")",$params['col'])[0];
		if(isset($result->id)){

			$result->detil_kerusakan = json_decode($result->detil_kerusakan,JSON_FORCE_OBJECT);
			
			$result->detil_permukaan = json_decode($result->detil_permukaan,JSON_FORCE_OBJECT);
			
			$result->detil_bahu_jalan = json_decode($result->detil_bahu_jalan,JSON_FORCE_OBJECT);
			
			$this->result['message'] = 'SUCCESS';
			$this->result["data"] = $result;
		}else{
			$this->result['message'] = 'Data not found';
		}
		
		return (object)$this->result;
	}

	public function get_kabupaten(){
		$res_wilayah = new wilayah;
		$result = $res_wilayah->get_list_kabupaten();
		return $result;
	}

	public function sync_field($dt){
		$key = [];
		foreach($dt as $elem => $value){
			if(in_array($elem ,$this->fillable)){
				$key[$elem] = $value;
			}
		}
		return $key;
	}
	

}
