<?php namespace Larasite\Model;

use Illuminate\Database\Eloquent\Model;
use Larasite\Library\FuncDB;
class detil_openstreet_model extends Model {

	Protected $tables = 'detil_konstruksi_jalan';
	Protected $hiddden = ['id'];
	protected $fillable = [];

	public function __construct(){
		$funcdb = new FuncDB;
		$res  = $funcdb->get_columns($this->tables);
		
		foreach($res as $key){
			if(in_array($res[$key], ['created_at', 'updated_at']))
				unset($res[$key]);
		}
		$this->columns = $res;
		$this->fillable = $this->columns; 
	}

	public function store($data){

	}
}
