<?php namespace Larasite;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Larasite\Library\FuncDB;
use Illuminate\Database\QueryException;

class UserTest extends Model implements AuthenticatableContract, CanResetPasswordContract {

	use Authenticatable, CanResetPassword;

	protected $table = 'users';
	Protected $procedures = ['register_user', 'user_login','cek_activation_users','user_logout','non_active_users'];
	protected $fillable = ['email','username','password','phone','remember_token'];
	protected $result = ["error"=>false,"data"=>null,"message"=>null,"code"=>null];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = ['password', 'remember_token'];

	public function getJWTIdentifier()
    {
        return $this->getKey();
    }
    public function getJWTCustomClaims()
    {
        return [];
    }

	public function register($data, $cb){
		$funcdb = new FuncDB;
		
		$params = $funcdb->get_param_procedure($this->procedures[0], $data);
		try {
			$result = \DB::SELECT("CALL ".$this->procedures[0]."(".$params['lenght'].")",$params['col']); // FULLL INPUT
			
			if($result[0]->code == 200)
				return $cb(false, $result);
			
			return $cb(true, $result[0]->message);
		
		} catch (QueryException $th) {
			return $cb(true, "Register failed.");
		}
	}

	public function login($data, $cb){
		$funcdb = new FuncDB;
		
		$params = $funcdb->get_param_procedure($this->procedures[1], $data);
		try {
			$result = \DB::SELECT("CALL ".$this->procedures[1]."(".$params['lenght'].")",$params['col']); // FULLL INPUT
			
			if(!isset($result[0]->code)){
				return $cb(false, $result);
			}
			
			return $cb(true, $result[0]->message);
		
		} catch (QueryException $th) {
			return $cb(true, $th->errorInfo);
		}
	}

	public function logout($data, $cb){
		$funcdb = new FuncDB;
		
		$params = $funcdb->get_param_procedure($this->procedures[3], $data);
		try {
			$result = \DB::SELECT("CALL ".$this->procedures[3]."(".$params['lenght'].")",$params['col']); // FULLL INPUT
			if($result[0]->code == 200) return $cb(false, $result[0]->message);
		
			return $cb(true, $result[0]->message);
		
		} catch (QueryException $th) {
			return $cb(true, $th->errorInfo);
		}
	}

	public function activation_user($data, $cb){
		$funcdb = new FuncDB;
		
		$params = $funcdb->get_param_procedure($this->procedures[2], $data);
		try {
			$result = \DB::SELECT("CALL ".$this->procedures[2]."(".$params['lenght'].")",$params['col']); // FULLL INPUT
			
			if($result[0]->code == 200)
				return $cb(false, $result);
			
			return $cb(true, $result[0]->message);
		
		} catch (QueryException $th) {
			return $cb(true, $th->errorInfo);
		}
	}

	public function non_active_users($data){
		$funcdb = new FuncDB;
		
		$params = $funcdb->get_param_procedure($this->procedures[4], $data);
		try {
			$result = \DB::SELECT("CALL ".$this->procedures[4]."(".$params['lenght'].")",$params['col']); // FULLL INPUT
			
			$this->result['message'] = $result[0]->message;
			$this->result['code'] = $result[0]->code;
			if($result[0]->code == 200){
				$this->result['error'] = true;
			}
			
			return (object)$this->result;
		
		} catch (QueryException $th) {
			$this->result['message'] = $th->errorInfo;
			$this->result['error'] = true;
			$this->result['code'] = 500;
			return (object)$this->result;
		}
	}
}
