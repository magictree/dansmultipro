<?php namespace Larasite\Library{

use Larasite\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\ServiceProvider as IlluminateServiceProvider;

	class pool_update{
		public function schedule($js,$EXP_WHOIAM,$RES,$TYP_ID,$comment_id){
        	try {
				$super  =  \DB::SELECT("select * from view_nonactive_login  where  lower(role_name) = 'superuser' and employee_id  =  '$EXP_WHOIAM' ");
		        $sup  = \DB::SELECT("select * from emp_supervisor where employee_id = '$RES[name]' and supervisor = '$EXP_WHOIAM' ");
		        $hr = \DB::SELECT("select distinct job.title, emp.employee_id from emp, job_history, job where emp.employee_id = job_history.employee_id and job.id =  job_history.job and emp.employee_id = '$EXP_WHOIAM' and ((lower(job.title) like '%hr%') or (lower(job.title) like '%human resource%') or (lower(job.title) like '%human%'))");
		        $it_director = \DB::SELECT("select distinct job.title, emp.employee_id from emp, job_history, job where emp.employee_id = job_history.employee_id and job.id =  job_history.job and emp.employee_id = '$EXP_WHOIAM' and ((lower(job.title) like '%it director%') or (lower(job.title) like '%it manager%'))");
		        $user = null;

		        if($sup != null){ $user = 'sup'; }
		        elseif($hr != null){ $user = 'hr'; }
		        elseif($super != null){ $user = 'hr'; }
		        elseif($it_director != null){ $user = 'hr'; }

		        if($js){ $model = "success"; }
		        
		        if($RES['name'] != $EXP_WHOIAM){                                    
		            $js['req_flow']['employee']         = $RES['name'];
		            $js['req_flow']['employee_approve'] ='o';
		            $js['req_flow']['employee_dates']   = null;
		            $js['req_flow']['employee_times']   = null;
		            $js['req_flow']['employee_requestor']   = [$EXP_WHOIAM,$user];
		        }

		       
		        $idx = array_search($EXP_WHOIAM,$js['hrx_comp']);
		        if(gettype($idx) != 'integer'){ $idx = array_search($EXP_WHOIAM,$js['supx_comp']); }
		        //return [$idx,'tets1'];
		        if((gettype($idx) == 'integer' || $idx == 0) && $user && $EXP_WHOIAM != '20148888'){
		            $dt = date("Y-m-d H:i:s a");
		            $exp = explode(' ',$dt);
		            $js[$user][$idx][$EXP_WHOIAM] = 1;
		            $js[$user][$idx][$user.'_stat'] = 1;
		            $js[$user][$idx][$user.'_date'] = $exp[0];
		            $js[$user][$idx][$user.'_time'] = $exp[1]." ".$exp[2];
		            $js[$user][$idx]['read_stat'] = 1;
		            if(in_array($TYP_ID, [1,2,3,4,5,6,7,8,9])){
		            	$js['req_flow'][$user.'_approve'] = "o";
		            }else{
		            	$js['req_flow'][$user.'_approve'] = "x";
		            }
		        }
		        $json_x = json_encode($js,1);


		        $get_pool =  \DB::SELECT("update pool_request set json_data = '$json_x'  where id_req = $comment_id  and master_type = 1 and type_id = $TYP_ID and employee_id = '".$RES['name']."'");
        	} catch (Exception $e) {
        		return $e->getMessage();
        	}
		}
	}// END class
}