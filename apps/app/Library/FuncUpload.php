<?php namespace Larasite\Library{

use Larasite\Http\Requests;

use Illuminate\Http\Request;
use Larasite\Privilege;
use Larasite\Model\AddEmployee_Model;
use Illuminate\Support\ServiceProvider as IlluminateServiceProvider;

	class FuncUpload
	{
		public function upload($dir,$file,$ext){
			$CurrentExt = $file->getClientOriginalExtension();
			$date = date_create(); $data_uniqid = uniqid();
			if(in_array($CurrentExt,$ext['extend']) ){ // filter type yang support upload

				if($file->getClientSize() <= 10000000){

					$slice_FileName = pathinfo($file->getClientOriginalName(),PATHINFO_FILENAME); //SLICE CHAR
					$str = str_slug($slice_FileName,$separator = "_");
					$dir_move = storage_path($dir);
					$data = $ext['title']."_".date_timestamp_get($date).".".$CurrentExt; $msg = 'Upload Successfully.';	$status=200;

					return ['filename'=>$data,'path'=>$dir,'type'=>$CurrentExt,'size'=>$this->formatSizeUnits($file->getClientSize()),'message'=>$msg,'status'=>$status];
				}else{ $msg = 'Max Size <= 2MB '; $data = null; $status=200; return ['filename'=>$data,'message'=>$msg]; }
			}else{ $msg = "Type not support $CurrentExt file"; $data = null; $status = 500; return ['status'=>$status,'type'=>$CurrentExt,'message'=>$msg,'size'=>$this->formatSizeUnits($file->getClientSize())]; }
		}
		/* MOVE FILE TO TRASH */
		public function move_file($file,$table){
			$get_data = \DB::select("SELECT `filename`, `path` from $table where filename = '$file' ");
			foreach ($get_data as $key) { $data['path'] = $key->path; $data['filename'] = $key->filename; }
			
			if(\Storage::disk('local')->exists($data['path']."/".$data['filename'])){
				\Storage::disk('local')->move($data['path']."/".$data['filename'], "/Trash/".$data['filename']);
				if( \Storage::disk('local')->exists("/Trash/".$data['filename']) ){ $message = 'Update Successfully.'; $status = 200; }
				else{ \Storage::disk('local')->move("/Trash/".$data['filename'],$data['path']."/".$data['filename']); $message = 'gagal move file to trash'; $status = 500; }
			}
			else{ $message = 'File tidak ditemukan, atau file telah dihapus check folder trash'; $status = 404; }
			
			return ['message'=>$message,'status'=>$status];
		}
		/* CONVERT SIZE FILE */
		private function formatSizeUnits($bytes)
	    {
	        if ($bytes >= 1073741824){	$bytes = number_format($bytes / 1073741824, 2) . ' GB';	}
	        elseif ($bytes >= 1048576){	$bytes = number_format($bytes / 1048576, 2) . ' MB';	}
	        elseif ($bytes >= 1024){	$bytes = number_format($bytes / 1024, 2) . ' KB';}
	        elseif ($bytes > 1){	$bytes = $bytes . ' bytes';}
	        elseif ($bytes == 1){	$bytes = $bytes . ' byte';}
	        else{	$bytes = '0 bytes'; }
	        return $bytes;
		}/* --- END METHOD FILE ---*/
		public function Sizefile($bytes)
	    {
	        if ($bytes >= 1073741824){	$bytes = number_format($bytes / 1073741824, 2);	}
	        elseif ($bytes >= 1048576){	$bytes = number_format($bytes / 1048576, 2);	}
	        elseif ($bytes >= 1024){	$bytes = number_format($bytes / 1024, 2);}
	        elseif ($bytes > 1){	$bytes = $bytes . ' bytes';}
	        elseif ($bytes == 1){	$bytes = $bytes . ' byte';}
	        else{	$bytes = '0 bytes'; }
	        return $bytes;
		}/* --- END METHOD FILE ---*/
	}
}