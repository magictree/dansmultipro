<?php namespace Larasite\Library{

use Larasite\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\ServiceProvider as IlluminateServiceProvider;

class LibRegex {     
    
    private static $instance = null;

    public static function get_instance()
    {

        $reg = [
            'name'=>'Regex:/^(?![\s.]+$)[0-9a-zA-Z\s.\@\_\-\.]*$/',
            'address'=>'Regex:/^(?![\s.]+$)[a-zA-Z\s.]*$/',
            'phone'=>'regex:/^[0-9 \- \+]{11,15}$/',
            'sta'=>'Regex:/^(?![\s.]+$)[a-zA-Z0-9 \- \+]*$/',
            'dms'=>'regex:(\d+)\s?\°\s?(\d+)\s?\'\s?(\d{1,}\.?\,?\d{0,}?)\"\s?(N|W|S|E)',
            'latlon'=>'regex:/^[0-9 \-,.]+$/',
        ];
        if (self::$instance == null) {
            self::$instance = $reg;
        } 

        // Kembalikan.
        return self::$instance;
    }

}
}