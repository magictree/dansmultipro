<?php namespace Larasite\Library{

use Larasite\Http\Requests;

use Illuminate\Http\Request;
use Larasite\Privilege;
use Larasite\Model\AddEmployee_Model;
use Illuminate\Support\ServiceProvider as IlluminateServiceProvider;

	class FuncAccess
	{

		private function set_valid($input)
		{
			$reg = ['text_num'=>'Regex:/^[A-Za-z0-9\-! ,\'\"\/@\.:\(\)]+$/',
					'text'=>'Regex:/^[A-Za-z\-! ,\'\"\/@\.:\(\)]+$/',
					'num'=>'Regex:/^[0-9-\^ ]+$/',
					'twit'=>'Regex:/^[A-Za-z0-9_]{1,15}$/'];

			$rule = ['req'=>'required|'.$reg['num'].'|max:7'
					];
			$valid = \Validator::make($input,$rule);
			return $valid;
		}
		public function Access($req,$page,$type){
			$get_role = $this->check($req);
			//return [$get_role,$req['key']];
			if($get_role['message'] == 'ACCESS DENIED !'){
				return ['Unauthorized',500,NULL,['create'=>0,'read'=>0,'update'=>0,'delete'=>0]];
			}
			$data_access = $this->check_role($get_role['data']['role'],$get_role['data']['employee_id'],$page);
			
			$param = ['create'=>$data_access['create'],'read'=>$data_access['read'],'update'=>$data_access['update'],'delete'=>$data_access['delete']];
			
			if(isset($get_role['data'])){
				if(isset($data_access[$type]) && $data_access[$type] != 0){
					$data = ['Access Granted',200,NULL,$param];
				}else{ $data = ['Unauthorized',500,NULL,$param]; }
			}else{ $data = [$get_role['message'],401,NULL,$param]; }
			return $data;
		}
		
		public function AccessPersonal($req,$pageArray,$type){	

			if(isset($req['Personal'])){
				$valid = $this->set_valid(['req'=>$req['Personal']]);
				if($valid->fails()){
					//return $valid->errors()->all();
					return ['Unauthorized2',403,null];
				}
			 }
			$get_role = $this->check($req['Request']);
			$emp = $get_role['data']['employee_id'];
			$hr = \DB::SELECT("select ldap.employee_id from ldap, role where ldap.employee_id='$emp' and ldap.role_id = role.role_id and (lower(role.role_name) like '%human%' or  '%human resource%' or '%hr%')");
			//return [$hr];
			
			$Permission = ['employee_id'=>$get_role['data']['employee_id'],
							'role_id'=>$get_role['data']['role'],
							'action'=>$type,
							'form'=>$pageArray];
			if(isset($req['Personal'])){ $Personal = $req['Personal']; }
			else{ $Personal = NULL; }
			$access = $this->check_type2($Personal,$Permission,$emp);
			
			if(isset($get_role['data']['role'])){
				if(isset($access['status']) && $access['status'] == 200){ // CHECK ACCESS
					$data = ['Access Granted',200,[$get_role['data']['role'],$get_role['data']['employee_id'], $access['message']['employee_id']], $access['crud']];
				}else{ $data = ['Unauthorized',403,NULL]; } // this issue
			}else{ $data = [$get_role['message'],401,NULL]; }
			return $data;	
		}

		// public function AccessList($req,$pageArray,$type)
		// {	
		// 	$get_role = $this->check($req['Request']);
		// 	$Permission = ['employee_id'=>$get_role['data']['employee_id'],
		// 					'role_id'=>$get_role['data']['role'],
		// 					'action'=>$type,
		// 					'form'=>$pageArray];
		// 	if(isset($req['Personal'])){ $Personal = $req['Personal']; }
		// 	else{ $Personal = NULL; }
		// 	$access = $this->check_typeList($Personal,$Permission);
		// 	if(isset($get_role['data']['role'])){
		// 		if(isset($access['status']) && $access['status'] == 200){ // CHECK ACCESS
		// 			$data = ['Access Granted',200,[$get_role['data']['role'],$get_role['data']['employee_id'], $access['message']['employee_id']]];
		// 		}else{ $data = ['Unauthorized',403,NULL]; }
		// 	}else{ $data = [$get_role['message'],401,NULL]; }
		// 	return $data;	
		// }

	/*METHOD CHECK ROLE
		* Paramater role_id && form_id
		*/
		public function check_role($role,$employee_id,$form)
		{
			$getModel = new Privilege;
			if(isset($role, $form)){ return $getModel->check_role_personal2($role,$employee_id,$form); }
			else{ $msg['warning'] = 'Unauthorized'; $msg['status'] = 404; return $msg; }
		}
	
	// CHECK LOCAL IT 
		private function check_local($employee_id,$role){
				$get = $this->Get_Type($role,$employee_id);
				if($get){
					foreach ($get as $key) {
						$type['local_it'] = $key->local_it;
						$type['employee_id'] = $key->employee_id;
					}	
				}else{
					$type = null;	
				}
			return $type;
		} 
		// END CHECK LOCAL IT
	// GET TYPE ID 
		public function Get_Type($role,$employee_id){
			$data = \DB::select("SELECT c.local_it, c.employee_id 
								FROM ldap b, emp c 
								WHERE c.employee_id = b.employee_id and 
								b.role_id = '$role' and c.employee_id = '$employee_id'");
			if(!isset($data)){ $data = null; }
			return $data;
		}
		private function check_localPersonal($Personal){
				$get = $this->Get_TypePersonal($Personal);
				if($get){
					foreach ($get as $key) { $type['local_it'] = $key->local_it; }	
				}else{ $type = null;	}
			return $type;
		}
		public function Get_TypePersonal($Personal){
			$data = \DB::select("SELECT local_it FROM emp WHERE employee_id = '$Personal'");
			if(!isset($data)){ $data = null; }return $data;
		}
		
	// CHECK WITHOUT LOCAL
		private function check($req){
			$getModel = new Privilege;	$r = array();
			if(isset($req['key'])){ // check isset $req
				$q = $getModel->session_key($req['key']);
				if(!empty($q)){ //check isset key in session
				//#########################################################################################
							 foreach ($q as $keys) {
							 	$r['key'] = $keys->session_key;
							 } // end for store role to array.
								$decode = base64_decode($r['key']);
								$employee_id = substr($decode,strpos($decode,'-')+1,strpos($decode,'-'));
								$key = substr($decode,0,strpos($decode,'-'));
								//return $employee_id;
								 $q2 = $getModel->session_role($employee_id);
								 foreach ($q2 as $keys) {
								 	$r['employee_id'] = $keys->employee_id;
								 	$r['role_id'] = $keys->role_id;
								 } // end for store role to array.				
				//#########################################################################################	
					if($r['employee_id'] == $employee_id){ // if type decode or encode
						$datas = \DB::select("select b.role_id from emp a, ldap b where a.employee_id = b.employee_id and b.employee_id = $employee_id and b.role_id = $r[role_id] ");
						foreach ($datas as $key) { $role['role'] = $key->role_id; }
						if(isset($role['role'])){
							$data =  ['role'=>$role['role'],'employee_id'=>$r['employee_id']]; $msg = 'ACCESS GRANTED !';	
						}else{ $msg = 'ACCESS DENIED!'; $data = null; }
					}
					else{ $msg = 'ACCESS DENIED!'; $data = null;}
				}
				else{ $msg = 'ACCESS DENIED !'; $data = null;}
			}// end if isset $req
			else{$msg = 'ACCESS DENIED, KEY URL NOT FOUND !'; $data = null; }
			return ['message'=>$msg,'data'=>$data];
		}

	/* CHECK TYPE AND ACCESS ########################################################################
		* CHECK TYPE ID EMPLOYEE EXPAT OR LOCAl OR LOCAL_IT
		* ACTION = 1(CREATE & READ), 2(EDIT & UPDATE), 3(DESTROY)
		*/
		// private function check_typeList($Personal,$Permission) // CHECK ROLE FIX***
		// {
		// 	$status_error = 403; $not_access = 'Unauthorized';
		// 	$employee_id = $Permission['employee_id'];
		// 	$role = $Permission['role_id'];
		// 	$form = $Permission['form'];
		// 	$action = $Permission['action'];

		// 	if(isset($Personal)){ $type['data'] = $this->check_localPersonal($Personal); }
		// 	else{ $type['data'] =  $this->check_local($employee_id,$role); }

		// 	$type['local_it'] = $type['data']['local_it'];

		// 	if($type['local_it']){
		// 		if($type['local_it'] == 1){ $crud = $this->Crud($employee_id,$role,$form['expat'],$action); $message=$crud[0]; $status=$crud[1]; } 
		// 		elseif($type['local_it'] == 2){ $crud = $this->Crud($employee_id,$role,$form['local'],$action); $message=$crud[0]; $status=$crud[1]; }
		// 		elseif($type['local_it'] == 3){ $crud = $this->Crud($employee_id,$role,$form['local_it'],$action); $message=$crud[0]; $status=$crud[1]; }
		// 		else{ $message = $not_access; $status = $status_error; }
		// 	} 
		// 	else{ $message = $not_access; $status = $status_error; }
		// 	return ['message'=>$message,'status'=>$status];
		// } // CHECK TYPE AND ACCESS

		private function check_type2($Personal,$Permission,$id_req) // CHECK ROLE FIX***
		{	
				$status_error = 403; $not_access = 'Unauthorized';
				$employee_id = $Permission['employee_id'];
				$role = $Permission['role_id'];
				$form = $Permission['form'];
				$action = $Permission['action'];

				$crud = [null,null,null];

				if(isset($Personal)){ $type['data'] = $this->check_localPersonal($Personal); }
				else{ $type['data'] =  $this->check_local($employee_id,$role); }

				$type['local_it'] = $type['data']['local_it'];

				if($type['local_it']){

					

						$hr = \DB::SELECT("select ldap.employee_id from ldap, role where ldap.employee_id='$id_req' and ldap.role_id = role.role_id and (lower(role.role_name) like '%human%' or  '%human resource%' or '%hr%' or '%superuser%')");
						if($id_req != $Personal){

							if($type['local_it'] == 1){ $forms = $form['expat']; }
							elseif($type['local_it'] == 2){ $forms = $form['local']; }
							elseif($type['local_it'] == 3){ $forms = $form['local_it']; }

							if($role && $forms){							
								$perm = \DB::SELECT("select * from permissions where role_id=$role and form_id = $forms");
								if(count($perm)>0){
									//if($perm[0]->create == 0 && $perm[0]->update == 0 && count($hr) == 0){
									if($perm[0]->create == 0 && $perm[0]->update == 0 && $perm[0]->read == 0 && count($hr) == 0){
										return ['Unauthorized',403,null];			
									}
								}
							}else{ return ['Unauthorized',500,null];}
						}

						if($type['local_it'] == 1){ $crud = $this->Crud($employee_id,$role,$form['expat'],$action); $message=$crud[0]; $status=$crud[1]; } 
						elseif($type['local_it'] == 2){ $crud = $this->Crud($employee_id,$role,$form['local'],$action); $message=$crud[0]; $status=$crud[1]; }
						elseif($type['local_it'] == 3){ $crud = $this->Crud($employee_id,$role,$form['local_it'],$action); $message=$crud[0]; $status=$crud[1]; }
						else{ $message = $not_access; $status = $status_error; }
					
				}else{ $message = $not_access; $status = $status_error; }
				return ['message'=>$message,'status'=>$status,'crud'=>$crud[2]];
			
			
		} // CHECK TYPE AND ACCESS
		
		private function Crud($employee_id,$role,$form,$action)
		{	$status_error = 403; $not_access = 'Unauthorized';
			/*
			* 1[Read], 2[Create], 3[Update], 4[Delete]
			*/

			$check = $this->check_role($role,$employee_id,$form);
			if( $check['create'] != 0 && isset($check['create']) && isset($action) == 2){ // CREATE
				$message = ['employee_id'=>$check['employee_id'],'local_it'=>$check['local_it']]; $status =200;
			}
			elseif( $check['read'] == 1 && $check['create'] == 0 && $check['update'] == 0 && $check['delete'] == 0 && $action == 1){ // show for user UPDATE
				$message = ['employee_id'=>$check['employee_id'],'local_it'=>$check['local_it']]; $status =200;
			}
			elseif($check['update'] != 0 && isset($check['read'],$check['update']) && isset($action) == 3){ // UPDATE
				$message = ['employee_id'=>$check['employee_id'],'local_it'=>$check['local_it']]; $status =200;
			}
			elseif($check['delete'] != 0 && isset($check['delete']) && isset($action) == 4){ // DESTROY
				$message = ['employee_id'=>$check['employee_id'],'local_it'=>$check['local_it']]; $status =200;
			}
			else{ $message = $not_access; $status=$status_error;}	// ERROR ACCESS
			return [$message,$status,$check];
		}

	}
}