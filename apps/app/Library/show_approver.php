<?php namespace Larasite\Library{

use Larasite\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\ServiceProvider as IlluminateServiceProvider;

class show_approver{
	public function show_data($json, $input){
		$flows = $json['req_flow'];

		$flow_approval = [];
		$expat = false;
		if(isset($json['local_it'])){ 
			if($json['local_it'] == 'expat'){ $expat = true; }
		}

		$compare_times = ["sup"=>null,"hr"=>null,"employee"=>null];

		foreach ($flows as $keys => $values) {
			if($values != 0 && $values != "o" && $keys != 'employee_dates' && $keys != 'employee_times' && $keys != 'employee_requestor'){

				$arr = ["job_approval"=>strtoupper($keys),"name"=>null,"date"=>null,"time"=>null,"status"=>'Pending'];
				
				if($keys == 'employee'){
					if($flows['employee_approve'] == 'x'){

						$empids = $flows['employee'];
						//$select_emp  = \Db::SELECT("select concat(first_name,' ',middle_name,' ',last_name) as name_approval from emp where employee_id =  '$empids' ")[0]->name_approval;
						$select_emp = \DB::SELECT("CALL find_approval('$empids')")[0]->name_approval;
						$arr['name'] = $select_emp;
						$arr['date'] = $flows['employee_dates'];
						$arr['time'] = strtoupper($flows["employee_times"]);
						$arr['status'] = 'Approval';
						$compare_times['employee'] = strtotime(date($flows['employee_dates']." ".explode(" ",$flows["employee_times"])[0]));
						
					}else if($flows['employee_approve'] == 'v'){
						$empids = $flows['employee'];
						//$select_emp  = \Db::SELECT("select concat(first_name,' ',middle_name,' ',last_name) as name_approval from emp where employee_id =  '$empids' ")[0]->name_approval;
						$select_emp = \DB::SELECT("CALL find_approval('$empids')")[0]->name_approval;
						$arr['name'] = $select_emp;
						$arr['date'] = $flows['employee_dates'];
						$arr['time'] = strtoupper($flows["employee_times"]);
						$arr['status'] = 'Reject';
						$compare_times['employee'] = strtotime(date($flows['employee_dates']." ".explode(" ",$flows["employee_times"])[0]));
					}
				}else{
					try{							
						if($flows[$keys."_approve"]=="x"){

							foreach ($json[$keys] as $nm_idx => $values_nm) {
								if($json[$keys][$nm_idx][$keys."_stat"] == 1){
									$keyx = key($json[$keys][$nm_idx]);
									
									//$select_emp  = \Db::SELECT("select concat(first_name,' ',middle_name,' ',last_name) as name_approval from emp where employee_id =  '$keyx' ")[0]->name_approval;
									$select_emp = \DB::SELECT("CALL find_approval('$keyx')")[0]->name_approval;
									$arr['name'] = $select_emp;
									$arr['date'] = $json[$keys][$nm_idx][$keys."_date"];
									$arr['time'] = strtoupper($json[$keys][$nm_idx][$keys."_time"]);
									$compare_times[$keys] = strtotime(date($json[$keys][$nm_idx][$keys."_date"]." ".explode(" ",$json[$keys][$nm_idx][$keys."_time"])[0]));
									$arr['status'] = 'Approval';
									
								}
							}
						}else if($flows[$keys."_approve"]=="v"){

							foreach ($json[$keys] as $nm_idx => $values_nm) {
								if($json[$keys][$nm_idx][$keys."_stat"] == 1){
									$keyx = key($json[$keys][$nm_idx]);

									//$select_emp  = \Db::SELECT("select concat(first_name,' ',middle_name,' ',last_name) as name_approval from emp where employee_id =  '$keyx' ")[0]->name_approval;
									$select_emp = \DB::SELECT("CALL find_approval('$keyx')")[0]->name_approval;
									$arr['name'] = $select_emp;
									$arr['date'] = $json[$keys][$nm_idx][$keys."_date"];
									$arr['time'] = strtoupper($json[$keys][$nm_idx][$keys."_time"]);
									$compare_times[$keys] = strtotime(date($json[$keys][$nm_idx][$keys."_date"]." ".explode(" ",$json[$keys][$nm_idx][$keys."_time"])[0]));
									$arr['status'] = 'Reject';
									
								}
							}
						}
					}catch(\Exception $e){

					}
				}

				$flow_approval[] = $arr;
			}
		}
		
		$requestor = null;

		if(isset($json['req_flow']['employee_requestor'])){
			if($json['req_flow']['employee_requestor'][1] != 'superuser'){					
				$requestor_job = strtoupper($json['req_flow']['employee_requestor'][1]);
				for ($i=0; $i < count($flow_approval); $i++) { 
					if($flow_approval[$i]['job_approval'] == $requestor_job){
						if(isset($json['local_it'])){ 
							if($json['local_it'] == "expat"){
								$requestor = $flow_approval[$i]['name']." (IT DIRECTOR)";
							}else{
								$requestor = $flow_approval[$i]['name']." (".$flow_approval[$i]['job_approval'].")";	
							}
						}else{
							$requestor = $flow_approval[$i]['name']." (".$flow_approval[$i]['job_approval'].")";
						}
						unset($flow_approval[$i]);	
					}
				}
			}else{
				$idsu = $json['req_flow']['employee_requestor'][0];
				$select_emp  	= \Db::SELECT("select distinct concat(IFNULL(emp.first_name,''),'',IFNULL(emp.middle_name,' '),'',IFNULL(emp.last_name,'')) as name_approval, role.role_name from emp, role, ldap where emp.employee_id =  '$idsu' and ldap.employee_id='$idsu' and role.role_id = ldap.role_id ")[0];
				
				$requestor 		=  $select_emp->name_approval." (".$select_emp->role_name.")";
			}
		}else{
			//if($compare_times['sup'] > $compare_times['hr']){ $sub = "SUP"; }else{ $sub = "HR"; }
			//$requestor_job = $sub;
			for ($i=0; $i < count($flow_approval); $i++) { 
				if($flow_approval[$i]['job_approval'] == 'EMPLOYEE'){
					$requestor = $flow_approval[$i]['name']." (".$flow_approval[$i]['job_approval'].")";
					unset($flow_approval[$i]);	
				}
			}
			
		}
		

		if(isset($json['local_it'])){ 
			if($json['local_it'] == "expat"){
				foreach ($flow_approval as $key => $value) {
					if($value['job_approval'] == "HR"){
						// only approver supervisor for expat leave
						$in_arr = ["Sick Leave","Maternity Leave","Paternity Leave","Bereavement Leave","Marriage Leave","offday_oncall","Accumulation Day Off","Emergency Leave","Suspension"];
						
						if(in_array($input['leave_type'], $in_arr)){
							unset($flow_approval[$key]);
						}else{
							$flow_approval[$key]['job_approval'] = "IT DIRECTOR";
						}
					}
				}
			}

		}
		return ["flow_app"=>$flow_approval,"request_by"=>$requestor];
	}//end function
}// END class
}