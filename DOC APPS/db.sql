-- --------------------------------------------------------
-- Host:                         
-- Server version:               10.1.32-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win32
-- HeidiSQL Version:             11.1.0.6116
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Dumping database structure for test_db
DROP DATABASE IF EXISTS `test_db`;
CREATE DATABASE IF NOT EXISTS `test_db` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `test_db`;

-- Dumping structure for table test_db.accounts
DROP TABLE IF EXISTS `accounts`;
CREATE TABLE IF NOT EXISTS `accounts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(30) NOT NULL,
  `email` varchar(30) NOT NULL,
  `password` text NOT NULL,
  `phone` varchar(30) DEFAULT NULL,
  `remember_token` text,
  `active` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

-- Dumping data for table test_db.accounts: ~6 rows (approximately)
DELETE FROM `accounts`;
/*!40000 ALTER TABLE `accounts` DISABLE KEYS */;
INSERT INTO `accounts` (`id`, `username`, `email`, `password`, `phone`, `remember_token`, `active`) VALUES
	(1, 'admin_update12349', 'agus.triadji@gmail.com', '$2y$10$cjor.0qy/zXyuETytNi/qO4', '0838-1234-123', NULL, 0),
	(2, 'admin12346', 'agus2.triadji@gmail.com', '$2y$10$0gI.zITavdy1MbMuWF3NoO.', '0838-1234-123', NULL, 1),
	(3, 'superadmin', 'agus3.triadji@gmail.com', '$2y$10$IHbMPNnysjbi2haUFKUKY.A2owmHD0u23KWeYpU6psUHScnUbQNrm', '0838-1234-123', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VybmFtZSI6InN1cGVyYWRtaW4iLCJlbWFpbCI6ImFndXMzLnRyaWFkamlAZ21haWwuY29tIiwicGhvbmUiOiIwODM4LTEyMzQtMTIzIiwic3ViIjozLCJpc3MiOiJodHRwOlwvXC9sb2NhbGhvc3Q6NTAwMFwvYXBpZGV2XC9hdXRoXC9sb2dpbiIsImlhdCI6IjE2MTAwMDgxNTciLCJleHAiOiIxNjEwMDExNzU3IiwibmJmIjoiMTYxMDAwODE1NyIsImp0aSI6IjBhNjdlYThmYWZjZTJlOTA5NmU5YmEwMTZiMmYzMzBmIn0.H6saBzvC8mMuz4LjoDWk3jsloBG-2zgWBNCXy79quew', 1),
	(6, 'superadmin2', 'mr.rudito@gmail.com', '$2y$10$5qfhUcs.Zau2.AigH8sfbughXnrffACdvz4ocZoRVQsl8cFDYkgcO', '0838-1234-123', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VybmFtZSI6InN1cGVyYWRtaW4yIiwiZW1haWwiOiJtci5ydWRpdG9AZ21haWwuY29tIiwicGhvbmUiOiIwODM4LTEyMzQtMTIzIiwic3ViIjo2LCJpc3MiOiJodHRwOlwvXC9sb2NhbGhvc3Q6NTAwMFwvYXBpZGV2XC9hdXRoXC9sb2dpbiIsImlhdCI6IjE2MTAwNDkyNDgiLCJleHAiOiIxNjEwMDUyODQ4IiwibmJmIjoiMTYxMDA0OTI0OCIsImp0aSI6IjI4ZTlmMGEyNTRlZGZmYThmOTNmMDFlMzZkYTFjZTg5In0.myhARrWzavvReEoplPzDfSqC-eSEx04htYasCkuJ_F8', 1),
	(7, 'superadmin3', 'mr3.rudito@gmail.com', '$2y$10$ekm8YgzEV8rctjWs5wMZEeU4QdqN2UaWhuSPI5q6tv/5DlJEhnpam', '0838-1234-123', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VybmFtZSI6InN1cGVyYWRtaW4zIiwiZW1haWwiOiJtcjMucnVkaXRvQGdtYWlsLmNvbSIsInBhc3N3b3JkIjoiJDJ5JDEwJGVrbThZZ3pFVjhyY3RqV3M1d01aRWVVNFFkcU4yVWFXaHVTUEk1cTZ0dlwvNURsSkVobnBhbSIsInBob25lIjoiMDgzOC0xMjM0LTEyMyIsImlzcyI6Imh0dHA6XC9cL2xvY2FsaG9zdDo1MDAwXC9hcGlkZXZcL2F1dGhcL3JlZ2lzdGVyZWQiLCJpYXQiOiIxNjEwMDM5ODM3IiwiZXhwIjoiMTYxMDA0MzQzNyIsIm5iZiI6IjE2MTAwMzk4MzciLCJqdGkiOiJkM2UyYTUwMWFhNmYwZTFkMjFmNTk3NDRiMGRjNDE0MyJ9.pKY5MNGmCJU7JKTLqN1K-3lQEZ9WvHcGmEAVl1P--sk', 1),
	(8, 'superadmin12345', 'agus5.triadji@gmail.com', '$2y$10$w0bu0NKl6Y62.7VP3vLsfOX6wPE8dZ8plWYExSbZpWEUjJVoWClG.', '0838-1234-123', NULL, 1);
/*!40000 ALTER TABLE `accounts` ENABLE KEYS */;

-- Dumping structure for procedure test_db.actived_account
DROP PROCEDURE IF EXISTS `actived_account`;
DELIMITER //
CREATE PROCEDURE `actived_account`(
	IN `_id` INT,
	IN `_type` INT
)
BEGIN
SET @cek = (SELECT id FROM accounts WHERE id = _id);

if(@cek)then
	
	update accounts set active = _type WHERE id = _id;
	SELECT "SUCCESS" AS `message` , 200 AS `code` FROM DUAL;
	
ELSE
	SELECT "ERROR::Data not found" AS `message` , 400 AS `code` FROM DUAL;
END if;

END//
DELIMITER ;

-- Dumping structure for procedure test_db.cek_activation_accounts
DROP PROCEDURE IF EXISTS `cek_activation_accounts`;
DELIMITER //
CREATE PROCEDURE `cek_activation_accounts`(
	IN `_username` VARCHAR(50),
	IN `_email` VARCHAR(50),
	IN `_token` TEXT
)
BEGIN
SET @cek = (SELECT COUNT(username) AS `data` FROM accounts WHERE username = _username AND email = _email AND remember_token = _token);

if(@cek > 0)then
	UPDATE accounts SET `active` = '1' WHERE username = _username AND email = _email AND remember_token = _token;
	
	SELECT 'SUCCESS' AS message , 200 AS 'code' FROM DUAL;
ELSE
	SELECT 'End Session' AS message , 500 AS 'code' FROM DUAL;
END IF;
END//
DELIMITER ;

-- Dumping structure for procedure test_db.create_account
DROP PROCEDURE IF EXISTS `create_account`;
DELIMITER //
CREATE PROCEDURE `create_account`(
	IN `_username` VARCHAR(30),
	IN `_email` VARCHAR(30),
	IN `_password` TEXT,
	IN `_phone` VARCHAR(30)
)
BEGIN
SET @cek = (SELECT id FROM accounts WHERE username = _username OR email = _email);

if(@cek)then
	SELECT "ERROR::DUPLICATE_DATA" AS `message` , 400 AS `code` FROM DUAL;
ELSE
	INSERT INTO accounts(`username`,`email`,`password`,`phone`) VALUE(_username,_email,_password,_phone);
	SELECT "SUCCESS" AS `message` , 200 AS `code` FROM DUAL;
END if;
END//
DELIMITER ;

-- Dumping structure for procedure test_db.get_list_account
DROP PROCEDURE IF EXISTS `get_list_account`;
DELIMITER //
CREATE PROCEDURE `get_list_account`(
	IN `_id` INT
)
BEGIN
if(_id = 0)then
	SELECT a.id, a.username, a.email, a.phone FROM `accounts` a WHERE a.active = 1;
ELSE
 SELECT a.id, a.username, a.email, a.phone FROM `accounts` a WHERE a.id = _id;
END if;
END//
DELIMITER ;

-- Dumping structure for procedure test_db.register_user
DROP PROCEDURE IF EXISTS `register_user`;
DELIMITER //
CREATE PROCEDURE `register_user`(
	IN `_username` VARCHAR(50),
	IN `_email` VARCHAR(50),
	IN `_password` TEXT,
	IN `_phone` VARCHAR(50),
	IN `_remember_token` TEXT
)
BEGIN
if(_phone = '0')then
SET @phone = NULL;
ELSE
SET @phone = _phone;
END IF;

INSERT INTO accounts(
`username`,`email`,`password`,`phone`,`remember_token`
)VALUE(
_username, _email,_password, @phone,`_remember_token`
);

SELECT 'SUCCESS' AS 'message', 200 AS 'code' FROM DUAL;
END//
DELIMITER ;

-- Dumping structure for procedure test_db.store_temp_job_api
DROP PROCEDURE IF EXISTS `store_temp_job_api`;
DELIMITER //
CREATE PROCEDURE `store_temp_job_api`(
	IN `_json` LONGTEXT
)
BEGIN

END//
DELIMITER ;

-- Dumping structure for procedure test_db.update_account
DROP PROCEDURE IF EXISTS `update_account`;
DELIMITER //
CREATE PROCEDURE `update_account`(
	IN `_id` INT,
	IN `_username` VARCHAR(30),
	IN `_email` VARCHAR(30),
	IN `_phone` VARCHAR(30)
)
BEGIN
SET @cek = (SELECT id FROM accounts WHERE id = _id);

if(@cek)then
	
	update accounts set username = _username, email = _email, phone = _phone WHERE id = _id;
	SELECT "SUCCESS" AS `message` , 200 AS `code` FROM DUAL;
	
ELSE
	SELECT "ERROR::Data not found" AS `message` , 400 AS `code` FROM DUAL;
END if;
END//
DELIMITER ;

-- Dumping structure for procedure test_db.user_login
DROP PROCEDURE IF EXISTS `user_login`;
DELIMITER //
CREATE PROCEDURE `user_login`(
	IN `_username` VARCHAR(50)
)
BEGIN

END//
DELIMITER ;

-- Dumping structure for procedure test_db.user_logout
DROP PROCEDURE IF EXISTS `user_logout`;
DELIMITER //
CREATE PROCEDURE `user_logout`(
	IN `_username` VARCHAR(50),
	IN `_token` TEXT
)
BEGIN
SET @email_cek = (SELECT `email` FROM accounts WHERE username = _username AND remember_token = _token);

if(@email_cek IS NULL)then
	
	SELECT 'invalid credential1' AS 'message', 500 AS 'code' FROM DUAL;
ELSE
	
	UPDATE accounts SET remember_token = null WHERE username = _username;
	SELECT 'SUCCESS' AS 'message', 200 AS 'code' FROM DUAL;

END IF;
END//
DELIMITER ;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
